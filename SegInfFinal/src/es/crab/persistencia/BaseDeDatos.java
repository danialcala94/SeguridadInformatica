package es.crab.persistencia;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import es.crab.objetos.Usuario;

public class BaseDeDatos {
	
	final private static String DB_CONNECTION_SENTENCE = "jdbc:mysql://localhost:3306/" + BaseDeDatos.DB_NAME;
	final private static String DB_USERNAME = "root";
	final private static String DB_PASSWORD = "";
	final private static String DB_NAME = "seguridadinformatica";

	/**
	 * Obtiene todos los usuarios existentes en base de datos.
	 * @return Usuarios en base de datos.
	 */
	public static ArrayList<Usuario> getUsuarios() {
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		
		try {
			ResultSet rs = DriverManager.getConnection(DB_CONNECTION_SENTENCE, DB_USERNAME, DB_PASSWORD).prepareStatement("SELECT * FROM usuarios").executeQuery();
			while(rs.next())
				usuarios.add(new Usuario(rs.getInt("id"), rs.getString("nombre"), BigInteger.valueOf(rs.getLong("n")), BigInteger.valueOf(rs.getLong("e")), BigInteger.valueOf(rs.getLong("d")), BigInteger.valueOf(rs.getLong("p")), BigInteger.valueOf(rs.getLong("q"))));
		} catch (SQLException e) {
			System.out.println("[BD]: No se han podido obtener los usuarios.");
			e.printStackTrace();
			return null;
		}
		
		System.out.println("[BD]: Obtenidos " + usuarios.size() + " usuarios.");
		
		return usuarios;
	}
	
	/**
	 * Obtiene el usuario con el id indicado si existe en la base de datos. Si no, devuelve null.
	 * @param id Identificador del usuario para la base de datos.
	 * @return Usuario si existe en base de datos, null si no existe.
	 */
	public static Usuario getUsuario(int id) {
		Usuario usuario = null;
		
		PreparedStatement ps;
		try {
			ps = DriverManager.getConnection(DB_CONNECTION_SENTENCE, DB_USERNAME, DB_PASSWORD).prepareStatement("SELECT * FROM usuarios WHERE usuarios.id = ?;");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next())
				usuario = new Usuario(rs.getInt("id"), rs.getString("nombre"), BigInteger.valueOf(rs.getLong("n")), BigInteger.valueOf(rs.getLong("e")), BigInteger.valueOf(rs.getLong("d")), BigInteger.valueOf(rs.getLong("p")), BigInteger.valueOf(rs.getLong("q")));
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (usuario == null)
			System.out.println("[BD]: No se ha podido obtener el usuario con id " + id + ".");
		else
			System.out.println("[BD]: Obtenido el usuario con id " + usuario.getId() + ".");
		
		return usuario;
	}
	
	/**
	 * Elimina el usuario con el id indicado si existe en base de datos.
	 * @param id Identificador del usuario.
	 * @return TRUE si se ha borrado con �xito, FALSE si no ha sido posible borrarlo.
	 */
	public static boolean removeUsuario(int id) {
		Boolean borrado = false;
		
		if (BaseDeDatos.getUsuario(id) != null) {
			try {
				PreparedStatement ps = DriverManager.getConnection(DB_CONNECTION_SENTENCE, DB_USERNAME, DB_PASSWORD).prepareStatement("DELETE FROM usuarios WHERE usuarios.id = ?;");
				ps.setInt(1, id);
				ps.executeUpdate();
				
				borrado = true;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		if (borrado)
			System.out.println("[BD]: Eliminado el usuario con id " + id + ".");
		else
			System.out.println("[BD]: No se ha podido eliminar el usuario con id " + id + ".");
		
		return borrado;
	}
	
	/**
	 * Crea o modifica el usuario pasado por par�metro. Si ya exist�a, lo modifica. Si no, lo crea.
	 * @param usuario Usuario y datos de usuario a crear.
	 * @return Usuario creado en base de datos.
	 * @throws Exception 
	 */
	public static Usuario saveOrUpdateUsuario(Usuario usuario) throws Exception {
		// Buscamos el usuario en la base de datos si ya exist�a (si nos mandan id)
		Usuario usuariobd = null;
		
		if (usuario.getD() == null || usuario.getN() == null || usuario.getE() == null || usuario.getNombre() == null)
			throw new Exception("El usuario debe tener nombre, clave p�blica (n, e) y clave privada (d) para ser almacenado.");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			usuariobd = BaseDeDatos.getUsuario(usuario.getId());
			
			if (usuariobd != null) {
				// Si exist�a, lo actualizamos
				ps = DriverManager.getConnection(DB_CONNECTION_SENTENCE, DB_USERNAME, DB_PASSWORD).prepareStatement("UPDATE usuarios SET usuarios.n = ?, usuarios.e = ?, usuarios.d = ?, usuarios.p = ?, usuarios.q = ?, usuarios.nombre = ? WHERE usuarios.id = ?;");
				ps.setLong(1, usuario.getN().longValue());
				ps.setLong(2, usuario.getE().longValue());
				ps.setLong(3, usuario.getD().longValue());
				ps.setLong(4, usuario.getP().longValue());
				ps.setLong(5, usuario.getQ().longValue());
				ps.setString(6, usuario.getNombre());
				ps.setInt(7, usuario.getId());
				ps.executeUpdate();
				System.out.println("[BD]: Actualizado el usuario con id " + usuario.getId() + ".");
			} else {
				// Si no exist�a, lo creamos
				ps = DriverManager.getConnection(DB_CONNECTION_SENTENCE, DB_USERNAME, DB_PASSWORD).prepareStatement("INSERT INTO usuarios (n, e, d, p, q, nombre) VALUES (?, ?, ?, ?, ?, ?);");
				ps.setLong(1, usuario.getN().longValue());
				ps.setLong(2, usuario.getE().longValue());
				ps.setLong(3, usuario.getD().longValue());
				ps.setLong(4, usuario.getP().longValue());
				ps.setLong(5, usuario.getQ().longValue());
				ps.setString(6, usuario.getNombre());
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			System.out.println("[BD]: No se ha podido crear o modificar el usuario.");
			e.printStackTrace();
		}
			
		// Devolvemos el usuario de BD actualizado
		ArrayList<Usuario> usuarios = BaseDeDatos.getUsuarios();
		usuariobd = usuarios.get(usuarios.size() - 1);
		
		if (usuario.getId() != usuariobd.getId())
			System.out.println("[BD]: Se ha creado el usuario con id " + usuariobd.getId() + ".");
		
		return usuariobd;	
	}
	
}
