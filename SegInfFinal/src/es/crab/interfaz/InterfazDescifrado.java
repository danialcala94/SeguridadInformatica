package es.crab.interfaz;

import java.awt.Color;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JTextPane;

import es.crab.algoritmos.Cifrado;
import es.crab.algoritmos.Descifrado;
import es.crab.aplicacion.Aplicacion;
import es.crab.objetos.Usuario;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextArea;

public class InterfazDescifrado {

	private JFrame frame;
	private JComboBox comboBox_Destinatario;
	private JComboBox comboBox_Cifrado;
	private ArrayList<Usuario> listaUsuarios;
	private JTextField textFieldCifrado;
	private JTextArea textArea;
	

	/**
	 * Create the application.
	 */
	public InterfazDescifrado() {
		initialize();
		aniadeUsuarios();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		listaUsuarios = Aplicacion.getListaUsuarios();
		frame = new JFrame();
		frame.setBounds(100, 100, 515, 497);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().setBackground(new Color(255,239,213));
		frame.setResizable(false);
		frame.setTitle("Seguridad Inform�tica - CRAB");
		frame.setIconImage(new ImageIcon("recursos/imagenes/bloqueado.png").getImage());
		
		JLabel lblDestinatario = new JLabel("Destinatario:");
		lblDestinatario.setBounds(10, 11, 171, 14);
		frame.getContentPane().add(lblDestinatario);
		
		JLabel label = new JLabel("Alg cifrado sim\u00E9trico:");
		label.setBounds(10, 51, 171, 14);
		frame.getContentPane().add(label);
		
		JLabel lblNewLabel = new JLabel("Mensaje cifrado:");
		lblNewLabel.setBounds(10, 131, 105, 14);
		frame.getContentPane().add(lblNewLabel);
		
		comboBox_Destinatario = new JComboBox();
		comboBox_Destinatario.setBounds(197, 8, 302, 20);
		frame.getContentPane().add(comboBox_Destinatario);
		
		comboBox_Cifrado = new JComboBox();
		comboBox_Cifrado.addItem("Cifrado Cesar");
		comboBox_Cifrado.addItem("Cifrado Vigen�re");
		comboBox_Cifrado.addItem("Cifrado monoalfabetico");
		comboBox_Cifrado.addItem("Cifrado de Hill");
		comboBox_Cifrado.addItem("Ninguno");
		comboBox_Cifrado.setBounds(197, 48, 302, 20);
		frame.getContentPane().add(comboBox_Cifrado);
		
		JButton btnDescifrar = new JButton("Descifrar");
		btnDescifrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				descifraMensaje();
			}
		});
		btnDescifrar.setBounds(270, 127, 89, 23);
		frame.getContentPane().add(btnDescifrar);
		
		JLabel lblNewLabel_1 = new JLabel("Clave simetrica Cifrada");
		lblNewLabel_1.setBounds(10, 91, 171, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		textFieldCifrado = new JTextField();
		textFieldCifrado.setBounds(197, 88, 302, 20);
		frame.getContentPane().add(textFieldCifrado);
		textFieldCifrado.setColumns(10);
		
		textArea = new JTextArea();
		textArea.setBounds(10, 156, 414, 301);
		textArea.setLineWrap(true);
		JScrollPane sp = new JScrollPane(textArea);
		sp.setLocation(10, 156);
		sp.setSize(489,301);
		frame.getContentPane().add(sp);
		frame.setVisible(true);
	}
	
	private void aniadeUsuarios() {
		for(int i=0; i<listaUsuarios.size(); i++) {
			comboBox_Destinatario.addItem(listaUsuarios.get(i).getNombre());
		}
	}
	
	public void descifraMensaje() {
		String msj_cifrado = textArea.getText();
		String alfabeto = Aplicacion.getAlfabeto();
		ArrayList<BigInteger> claveDescifrada = null;
		if(comboBox_Cifrado.getSelectedItem().toString() != "Ninguno") {
			 claveDescifrada = descifraClave(textFieldCifrado.getText());
			 if(claveDescifrada == null) {
				 return;
			 }
		}
		String claro="";
		if(msj_cifrado != "" && msj_cifrado !=null) {
			switch (comboBox_Cifrado.getSelectedItem().toString()) {
			case "Cifrado Cesar":
				claro = Descifrado.Cesar(alfabeto, claveDescifrada, msj_cifrado);
				break;
				
			case "Cifrado Vigen�re":
				claro = Descifrado.Vigenere(claveDescifrada, msj_cifrado, alfabeto);
				break;
				
			case "Cifrado monoalfabetico":
				claro = Descifrado.Monoalfabetico(claveDescifrada, msj_cifrado, alfabeto);
				break;
				
			case "Cifrado de Hill":
				claro = Descifrado.Hill(msj_cifrado, alfabeto, claveDescifrada);
				break;
				
			case "Ninguno":
				//Cifrado normal con RSA en bloques
				Usuario usuario = listaUsuarios.get(comboBox_Destinatario.getSelectedIndex());
				claro = Descifrado.RSABloques(msj_cifrado, usuario, alfabeto);
				if(!claro.contains("[Cifrado CRAB]")) {
					JOptionPane.showMessageDialog(null, "Este mensaje no corresponde a este destinatario", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}else {
					claro = claro.substring(14);
				}
				break;
			}
			claro = sustituyeSaltos(claro);
			muestraMensajeDescifrado(claro);
		}else {
			JOptionPane.showMessageDialog(null, "No se puede descifrar un mensaje vacio", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private String sustituyeSaltos(String mensaje) {
		return mensaje.replace("*", "\n");
	}

	public void muestraMensajeDescifrado(String msj_claro) {
		String texto = "Se ha descifrado el mensaje utilizando el "+comboBox_Cifrado.getSelectedItem().toString()+".\nEl mensaje descifrado es:\n###############################\n\n";
		texto+=msj_claro;
		texto+="\n\n###############################";
		textArea.setText(texto);
	}
	
	public ArrayList<BigInteger> descifraClave(String claveCifrado) {
		Usuario usuario = listaUsuarios.get(comboBox_Destinatario.getSelectedIndex());
		String claveDescifrada = Descifrado.RSABloques(claveCifrado, usuario, Aplicacion.getAlfabeto());
		System.out.println("Clave Descifrada:" +claveDescifrada);
		 if(!claveDescifrada.contains("[Cifrado CRAB]")) {
			 JOptionPane.showMessageDialog(null, "Este mensaje no corresponde a este destinatario", "Error", JOptionPane.ERROR_MESSAGE);
			 return null;
		 }else {
			 claveDescifrada = claveDescifrada.substring(14);
		 }
		String[] claveDescifradaSeparada = claveDescifrada.split("-");
		ArrayList<BigInteger> listaClave = new ArrayList<BigInteger>();
		for(int i=0; i<claveDescifradaSeparada.length; i++) {
			listaClave.add(new BigInteger(claveDescifradaSeparada[i].trim()));
		}
		return listaClave;
	}
}
