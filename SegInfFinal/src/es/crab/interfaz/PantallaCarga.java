package es.crab.interfaz;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import es.crab.aplicacion.Aplicacion;

import java.awt.Font;

public class PantallaCarga extends JFrame {
	
	public PantallaCarga() {
		getContentPane().setBackground(new Color(255, 239, 213));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		setTitle("Seguridad Informática - CRAB");
		setIconImage(new ImageIcon("recursos/imagenes/bloqueado.png").getImage());
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Estamos realizando algunos ajustes iniciales...");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 11, 327, 33);
		getContentPane().add(lblNewLabel);
		
		JLabel imagen = new JLabel("New label");
		imagen.setBounds(141, 55, 50, 50);
		imagen.setIcon(new javax.swing.ImageIcon("recursos/imagenes/Cargando.gif"));
		getContentPane().add(imagen);
		
		setVisible(true);
		setBounds(300, 400, 355, 140);

		Aplicacion.iniciaAplicacion();
		System.out.println("Despues");
		
		InterfazPrincipal interfazPrincipal = new InterfazPrincipal();
		this.dispose();
	
	}
}
