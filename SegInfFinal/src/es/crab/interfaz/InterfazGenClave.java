package es.crab.interfaz;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import es.crab.aplicacion.Aplicacion;
import es.crab.objetos.Usuario;
import es.crab.persistencia.BaseDeDatos;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class InterfazGenClave extends JFrame {
	
	Aplicacion aplicacion = new Aplicacion();
	private JTextField textField_Nombre;
	private JTextField textField_Publica;
	private JTextField textField_Privada;
	private JTextField textField_n;
	
	private Usuario usuario;
	
	public InterfazGenClave() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		getContentPane().setBackground(new Color(255,239,213));
		setResizable(false);
		setTitle("Seguridad Inform�tica - CRAB");
		setIconImage(new ImageIcon("recursos/imagenes/bloqueado.png").getImage());
			
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 22, 80, 14);
		getContentPane().add(lblNombre);
		
		JLabel lblClavePublica = new JLabel("Clave publica:");
		lblClavePublica.setBounds(10, 62, 80, 14);
		getContentPane().add(lblClavePublica);
		
		JLabel lblClavePrivada = new JLabel("Clave privada:");
		lblClavePrivada.setBounds(10, 102, 80, 14);
		getContentPane().add(lblClavePrivada);
		
		JLabel lblN = new JLabel("n:");
		lblN.setBounds(10, 142, 80, 14);
		getContentPane().add(lblN);
		
		textField_Nombre = new JTextField();
		textField_Nombre.setBounds(100, 19, 165, 20);
		getContentPane().add(textField_Nombre);
		textField_Nombre.setColumns(10);
		
		textField_Publica = new JTextField();
		textField_Publica.setEditable(false);
		textField_Publica.setColumns(10);
		textField_Publica.setBounds(100, 59, 165, 20);
		getContentPane().add(textField_Publica);
		
		textField_Privada = new JTextField();
		textField_Privada.setEditable(false);
		textField_Privada.setColumns(10);
		textField_Privada.setBounds(100, 99, 165, 20);
		getContentPane().add(textField_Privada);
		
		textField_n = new JTextField();
		textField_n.setEditable(false);
		textField_n.setColumns(10);
		textField_n.setBounds(100, 139, 165, 20);
		getContentPane().add(textField_n);
		
		JButton btnGuardarUsuario = new JButton("Guardar usuario");
		btnGuardarUsuario.setEnabled(false);
		btnGuardarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (JOptionPane.showConfirmDialog(null, "�Desea crear el usuario " + textField_Nombre.getText() + "?", "Crear usuario", JOptionPane.INFORMATION_MESSAGE) == 0) {
						BaseDeDatos.saveOrUpdateUsuario(usuario);
						aplicacion.actualizarListaUsuarios();
						btnGuardarUsuario.setEnabled(false);
						textField_n.setText("");
						textField_Nombre.setText("");
						textField_Privada.setText("");
						textField_Publica.setText("");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnGuardarUsuario.setBounds(64, 234, 157, 23);
		getContentPane().add(btnGuardarUsuario);
		
		JButton btnGenerarClave = new JButton("Generar clave");
		btnGenerarClave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nombre = textField_Nombre.getText();
				if (nombre != null && nombre.length() > 0) {
					usuario = new Usuario(nombre);
					seteaCampos(usuario);
					btnGuardarUsuario.setEnabled(true);
				}else {
					JOptionPane.showMessageDialog(null, "No se puede generar un usuario con el campo nombre vacio", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnGenerarClave.setBounds(64, 184, 157, 23);
		getContentPane().add(btnGenerarClave);
		
		setVisible(true);
		setBounds(300, 400, 300, 297);
	}
	
	public void seteaCampos(Usuario usuario) {
		textField_Publica.setText(usuario.getE().toString());
		textField_Privada.setText(usuario.getD().toString());
		textField_n.setText(usuario.getN().toString());
	}
}