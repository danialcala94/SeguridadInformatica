package es.crab.interfaz;

import java.awt.Color;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import es.crab.algoritmos.Cifrado;
import es.crab.aplicacion.Aplicacion;
import es.crab.objetos.Usuario;
import es.crab.persistencia.BaseDeDatos;
import es.crab.utiles.GeneradorClavesSimetricas;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class InterfazCifrado {

	private JFrame frame;
	private JComboBox comboBox_Destinatario;
	private JComboBox comboBox_cifradoSim;
	ArrayList<Usuario> listaUsuarios;
	private JTextArea textArea;

	/**
	 * Create the application.
	 */
	public InterfazCifrado() {
		listaUsuarios = Aplicacion.getListaUsuarios();
		initialize();
		aniadeUsuarios();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {			
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 445);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().setBackground(new Color(255,239,213));
		frame.setResizable(false);
		frame.setTitle("Seguridad Inform�tica - CRAB");
		frame.setIconImage(new ImageIcon("recursos/imagenes/bloqueado.png").getImage());
		
		comboBox_Destinatario = new JComboBox();
		comboBox_Destinatario.setBounds(147, 11, 277, 20);
		frame.getContentPane().add(comboBox_Destinatario);
		
		comboBox_cifradoSim = new JComboBox();
		comboBox_cifradoSim.addItem("Cifrado Cesar");
		comboBox_cifradoSim.addItem("Cifrado Vigen�re");
		comboBox_cifradoSim.addItem("Cifrado monoalfabetico");
		comboBox_cifradoSim.addItem("Cifrado de Hill");
		comboBox_cifradoSim.addItem("Ninguno");
		comboBox_cifradoSim.setBounds(147, 51, 277, 20);
		frame.getContentPane().add(comboBox_cifradoSim);
		
		JLabel lblDestinatario = new JLabel("Destinatario:");
		lblDestinatario.setBounds(10, 14, 127, 14);
		frame.getContentPane().add(lblDestinatario);
		
		JLabel lblAlgCifradoSimtrico = new JLabel("Alg cifrado sim\u00E9trico:");
		lblAlgCifradoSimtrico.setBounds(10, 54, 127, 14);
		frame.getContentPane().add(lblAlgCifradoSimtrico);
		
		JLabel lblMensajeAEnviar = new JLabel("Mensaje a enviar");
		lblMensajeAEnviar.setBounds(10, 94, 127, 14);
		frame.getContentPane().add(lblMensajeAEnviar);
		
		JButton btnCifrar = new JButton("Cifrar");
		btnCifrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cifraMensaje();
			}
		});
		btnCifrar.setBounds(232, 85, 89, 23);
		frame.getContentPane().add(btnCifrar);
		
		textArea = new JTextArea();
		textArea.setBounds(10, 119, 424, 286);
		textArea.setLineWrap(true);
		JScrollPane sp = new JScrollPane(textArea);
		sp.setLocation(10, 119);
		sp.setSize(424,286);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		frame.getContentPane().add(sp);
		
		frame.setVisible(true);
	}
	
	private void aniadeUsuarios() {
		for(int i=0; i<listaUsuarios.size(); i++) {
			comboBox_Destinatario.addItem(listaUsuarios.get(i).getNombre());
		}
	}
	
	private void cifraMensaje() {
		String mensaje = textArea.getText();
		mensaje = sustituyeSaltos(mensaje);
		String alfabeto = Aplicacion.getAlfabeto();
		String cifrado = "";
		ArrayList<BigInteger> claveCifrado = null;
		Usuario usuario = listaUsuarios.get(comboBox_Destinatario.getSelectedIndex());
		if(mensaje != "" && mensaje !=null) {
			switch (comboBox_cifradoSim.getSelectedItem().toString()) {
			case "Cifrado Cesar":
				claveCifrado = GeneradorClavesSimetricas.generarClaveCesar(alfabeto);
				cifrado = Cifrado.Cesar(alfabeto, claveCifrado, mensaje);
				break;
				
			case "Cifrado Vigen�re":
				claveCifrado = GeneradorClavesSimetricas.generarClaveVigenere(alfabeto, mensaje);
				cifrado = Cifrado.Vigenere(claveCifrado, mensaje, alfabeto);
				break;
				
			case "Cifrado monoalfabetico":
				claveCifrado = GeneradorClavesSimetricas.generarClaveMonoalfabetico(alfabeto);
				cifrado = Cifrado.Monoalfabetico(claveCifrado, mensaje, alfabeto);
				break;
				
			case "Cifrado de Hill":
				claveCifrado = GeneradorClavesSimetricas.generarClaveHill(alfabeto);
				cifrado = Cifrado.Hill(mensaje, alfabeto, claveCifrado);
				break;
				
			case "Ninguno":
				//Cifrado normal con RSA en bloques
				cifrado = Cifrado.RSABloques("[Cifrado CRAB]"+mensaje, usuario, alfabeto);
				break;
			}
			//Ciframos la clave
			if(comboBox_cifradoSim.getSelectedItem().toString() != "Ninguno") {
				String ClaveCifrada = cifraClave(claveCifrado, usuario);
				muestraMensajeCifrado(cifrado, ClaveCifrada);
			}else {
				muestraMensajeCifrado(cifrado, null);
			}
		}else {
			JOptionPane.showMessageDialog(null, "No se puede generar un cifrar un mensaje vacio", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private String sustituyeSaltos(String mensaje) {
		return mensaje.replace("\n", "*");
	}

	public String cifraClave(ArrayList<BigInteger> claveCifrado, Usuario usuario) {
		System.out.println("Clave cifrado: "+claveCifrado.toString());
		String claveTexto = "";
		for(int i=0; i<claveCifrado.size(); i++) {
			if(i == claveCifrado.size()-1) {
				claveTexto += claveCifrado.get(i).toString();
			}else {
				claveTexto += claveCifrado.get(i).toString()+"-";
			}
		}
		claveTexto="[Cifrado CRAB]"+claveTexto;
		System.out.println("claveTexto:"+claveTexto);
		String claveCifrada = Cifrado.RSABloques(claveTexto, usuario, Aplicacion.getAlfabeto());
		return claveCifrada;
	}
	
	public void muestraMensajeCifrado(String msj_cifrado, String claveCifrada) {
		String texto = "";
		if(claveCifrada == null) {
			texto = "Se ha cifrado el mensaje utilizando RSA en bloques.\nEl mensaje cifrado es:\n###############################\n\n";
			texto += msj_cifrado;
			texto += "\n\n###############################\n";
		}else {
			texto = "Se ha cifrado el mensaje utilizando el "+comboBox_cifradoSim.getSelectedItem().toString()+".\nEl mensaje cifrado es:\n##############\n\n";
			texto+=msj_cifrado;
			texto+="\n\n###############################\nSe ha cifrado utilizando la clave:\n###############################\n\n";
			texto+=claveCifrada;
			texto+="\n\n###############################";
		}
		textArea.setText(texto);
	}
}