package es.crab.interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InterfazPrincipal {

	private JFrame frame;


	/**
	 * Create the application.
	 */
	public InterfazPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().setBackground(new Color(255,239,213));
		frame.setResizable(false);
		frame.setTitle("Seguridad Informática - CRAB");
		frame.setIconImage(new ImageIcon("recursos/imagenes/bloqueado.png").getImage());
		
		JButton btnInsertarUsuariosEn = new JButton("Insertar usuarios en el sistema");
		btnInsertarUsuariosEn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InterfazGenClave interfazGeneradorClaves = new InterfazGenClave();
			}
		});
		btnInsertarUsuariosEn.setBounds(92, 208, 251, 23);
		frame.getContentPane().add(btnInsertarUsuariosEn);
		
		JButton btnCifrarMensajes = new JButton("Cifrar mensajes");
		btnCifrarMensajes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InterfazCifrado interfazCifrado = new InterfazCifrado();
			}
		});
		btnCifrarMensajes.setBounds(92, 242, 251, 23);
		frame.getContentPane().add(btnCifrarMensajes);
		
		JButton btnDescifrarMensajes = new JButton("Descifrar mensajes");
		btnDescifrarMensajes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InterfazDescifrado interfazDescifrado = new InterfazDescifrado();
			}
		});
		btnDescifrarMensajes.setBounds(92, 276, 251, 23);
		frame.getContentPane().add(btnDescifrarMensajes);
		
		JLabel lblNewLabel = new JLabel("Aplicacion de cifrado de mensajes con cifrado mixto");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 11, 414, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblVariosAlgoritmosDe = new JLabel("Varios algoritmos de cifrado simetrico");
		lblVariosAlgoritmosDe.setHorizontalAlignment(SwingConstants.CENTER);
		lblVariosAlgoritmosDe.setBounds(10, 32, 414, 14);
		frame.getContentPane().add(lblVariosAlgoritmosDe);
		
		JLabel lblGeneracionAutomticasDe = new JLabel("Generacion autom\u00E1ticas de claves simetricas y RSA");
		lblGeneracionAutomticasDe.setHorizontalAlignment(SwingConstants.CENTER);
		lblGeneracionAutomticasDe.setBounds(10, 57, 414, 14);
		frame.getContentPane().add(lblGeneracionAutomticasDe);
		
		JLabel lblDanielAlcalValera = new JLabel("Daniel Alcal\u00E1 Valera");
		lblDanielAlcalValera.setHorizontalAlignment(SwingConstants.CENTER);
		lblDanielAlcalValera.setBounds(10, 108, 414, 14);
		frame.getContentPane().add(lblDanielAlcalValera);
		
		JLabel lblMartnBaynGutirrez = new JLabel("Mart\u00EDn Bay\u00F3n Guti\u00E9rrez");
		lblMartnBaynGutirrez.setHorizontalAlignment(SwingConstants.CENTER);
		lblMartnBaynGutirrez.setBounds(10, 133, 414, 14);
		frame.getContentPane().add(lblMartnBaynGutirrez);
		
		JLabel lblBeatrizCrespoTorbado = new JLabel("Beatriz Crespo Torbado");
		lblBeatrizCrespoTorbado.setHorizontalAlignment(SwingConstants.CENTER);
		lblBeatrizCrespoTorbado.setBounds(10, 158, 414, 14);
		frame.getContentPane().add(lblBeatrizCrespoTorbado);
		
		JLabel lblMaraRiaoSalio = new JLabel("Mar\u00EDa Ria\u00F1o Salio");
		lblMaraRiaoSalio.setHorizontalAlignment(SwingConstants.CENTER);
		lblMaraRiaoSalio.setBounds(10, 183, 414, 14);
		frame.getContentPane().add(lblMaraRiaoSalio);
		
		frame.setVisible(true);
	}
}
