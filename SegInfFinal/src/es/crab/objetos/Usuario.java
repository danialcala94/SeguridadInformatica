package es.crab.objetos;

import java.math.BigInteger;
import java.util.ArrayList;

import es.crab.aplicacion.Aplicacion;
import es.crab.utiles.Utiles;

public class Usuario {
	private int id = -3;
	private String nombre = null;
	private BigInteger n = null;
	private BigInteger e = null;
	private BigInteger d = null;
	private BigInteger p = null;
	private BigInteger q = null;
	
	/**
	 * Constructor para la base de datos. Recibe identificador.
	 * @param id Identificador del usuario en base de datos.
	 * @param nombre Nombre del usuario.
	 * @param n Primer valor de la clave p�blica.
	 * @param e Segundo valor de la clave p�blica.
	 * @param d Tercer valor de la clave p�blica.
	 */
	public Usuario(int id, String nombre, BigInteger n, BigInteger e, BigInteger d, BigInteger p, BigInteger q) {
		this.id = id;
		this.nombre = nombre;
		this.n = n;
		this.e = e;
		this.d = d;
		this.p = p;
		this.q = q;
	}
	
	/**
	 * Constructor para aplicaci�n. No recibe identificador.
	 * @param nombre Nombre del usuario.
	 * @param n Primer valor de la clave p�blica.
	 * @param e Segundo valor de la clave p�blica.
	 * @param d Tercer valor de la clave p�blica.
	 */
	public Usuario(String nombre, BigInteger n, BigInteger e, BigInteger d, BigInteger p, BigInteger q) {
		this.nombre = nombre;
		this.n = n;
		this.e = e;
		this.d = d;
		this.p = p;
		this.q = q;
	}
	
	/**
	 * Constructor para aplicaci�n. Recibe el nombre del usuario y genera autom�ticamente la clave p�blica, la clave privada y los primos que dan lugar a la clave p�blica.
	 * @param nombre Nombre del usuario a generar.
	 */
	public Usuario(String nombre) {
		this.nombre = nombre;
		// Calculamos n en base a dos n�meros primos
		ArrayList<BigInteger> primos = Aplicacion.getPrimos();
		BigInteger p = primos.get((int) (Math.random() * primos.size()));
		BigInteger q = primos.get((int) (Math.random() * primos.size()));
		this.n = p.multiply(q);
		// Calculamos un e que sea v�lido (mcd(n, e) = 1), es decir, si son primos entre s�.
		boolean eValido = false;
		while (!eValido) {
			this.e = primos.get((int) (Math.random() * primos.size()));
			if (Utiles.primosEntreSi(this.e, this.n))
				eValido = true;
		}
		this.p = p;
		this.q = q;
		try {
			this.d = Utiles.algoritmoEuclidesExtendido(e, Utiles.calcularPhi(p, q));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Asigna un identificador al usuario.
	 * @param id Identificador.
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Obtiene el identificador del usuario.
	 * @return Identificador.
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * Asigna un nombre de usuario al usuario.
	 * @param nombre Nombre de usuario.
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Devuelve el nombre de usuario del usuario.
	 * @return Nombre de usuario.
	 */
	public String getNombre() {
		return this.nombre;
	}
	
	/**
	 * Asigna un valor al primer elemento de la clave p�blica (n). La clave p�blica es (n, e).
	 * @param n Valor del primer elemento de la clave p�blica.
	 */
	public void setN(BigInteger n) {
		this.n = n;
	}
	
	/**
	 * Devuelve el valor del primer elemento de la clave p�blica (n). La clave p�blica es (n, e).
	 * @return Valor de n.
	 */
	public BigInteger getN() {
		return this.n;
	}
	
	/**
	 * Asigna un valor al segundo elemento de la clave p�blica (e). La clave p�blica es (n, e).
	 * @param e Valor del segundo elemento de la clave p�blica.
	 */
	public void setE(BigInteger e) {
		this.e = e;
	}
	
	/**
	 * Devuelve el valor del segundo elemento de la clave p�blica (e). La clave p�blica es (n, e).
	 * @return Valor de e.
	 */
	public BigInteger getE() {
		return this.e;
	}
	
	/**
	 * Asigna un valor a la clave privada (d).
	 * @param d Valor de la clave privada.
	 */
	public void setD(BigInteger d) {
		this.d = d;
	}
	
	/**
	 * Devuelve el valor de la clave privada (d).
	 * @return Valor de d.
	 */
	public BigInteger getD() {
		return this.d;
	}
	
	/**
	 * Asigna el primer n�mero primo divisor de n.
	 * @param p Primer n�mero primo divisor de n.
	 */
	public void setP(BigInteger p) {
		this.p = p;
	}
	
	/**
	 * Devuelve el primer n�mero primo divisor de n.
	 * @return Valor de p.
	 */
	public BigInteger getP() {
		return this.p;
	}
	
	/**
	 * Asigna el segundo n�mero primo divisor de n.
	 * @param p Segundo n�mero primo divisor de n.
	 */
	public void setQ(BigInteger q) {
		this.q = q;
	}
	
	/**
	 * Devuelve el segundo n�mero primo divisor de n.
	 * @return Valor de q.
	 */
	public BigInteger getQ() {
		return this.q;
	}
	
	/**
	 * Devuelve los datos del usuario en formato String.
	 */
	public String toString() {
		return this.nombre + " [" + this.id + "]: Clave p�blica (" + this.n + ", " + this.e + ") - Clave privada (" + this.d + ").";
	}
}
