package es.crab.objetos;

import java.math.BigInteger;
import java.util.ArrayList;

import es.crab.aplicacion.Aplicacion;

public class Mensaje {
	private String mensajeCifrado = null;
	private ArrayList<BigInteger> claveCifradaHill = new ArrayList<BigInteger>();
	private ArrayList<BigInteger> claveCifradaMonoalfabetico = new ArrayList<BigInteger>();
	private ArrayList<BigInteger> claveCifradaVigenere = new ArrayList<BigInteger>();
	private ArrayList<BigInteger> claveCifradaCesar = new ArrayList<BigInteger>();
	
	/**
	 * Mensaje cuya clave de cifrado se ha cifrado con RSABloques (con la clave p�blica del receptor del mensaje) y cuyo contenido se ha cifrado con la clave de cifrado dada como par�metro.
	 * @param mensajeCifrado Mensaje cifrado con la 'claveCifrada'.
	 * @param claveCifrada Clave de cifrado del mensaje, cifrada con RSABloques (con la clave p�blica del receptor del mensaje).
	 * @param idCifrado Identificador del m�todo de cifrado correspondiente a la 'claveCifrada'.
	 */
	public Mensaje(String mensajeCifrado, ArrayList<BigInteger> claveCifrada, int idCifrado) {
		this.mensajeCifrado = mensajeCifrado;
		switch (idCifrado) {
		case Aplicacion.HILL:
			this.claveCifradaHill = claveCifrada;
			break;
		case Aplicacion.MONOALFABETICO:
			this.claveCifradaMonoalfabetico = claveCifrada;
			break;
		case Aplicacion.VIGENERE:
			this.claveCifradaVigenere = claveCifrada;
			break;
		case Aplicacion.CESAR:
			this.claveCifradaCesar = claveCifrada;
			break;
		}
	}
	
	/**
	 * Devuelve el mensaje cifrado.
	 * @return Mensaje cifrado.
	 */
	public String getMensajeCifrado() {
		return this.mensajeCifrado;
	}
	
	/**
	 * Devuelve la clave de cifrado (si se ha utilizado un cifrado Hill, Monoalfab�tico, Vigen�re o C�sar).
	 * @return NULL si solo se aplic� RSABloques al contenido, o la clave del m�todo de cifrado utilizado.
	 */
	public ArrayList<BigInteger> getClaveCifrada() {
		ArrayList<BigInteger> clave = null;
		
		if (isHill())
			clave = this.claveCifradaHill;
		else if (isMonoalfabetico())
			clave = this.claveCifradaMonoalfabetico;
		else if (isVigenere())
			clave = this.claveCifradaVigenere;
		else if (isCesar())
			clave = this.claveCifradaCesar;
		
		return clave;
	}
	
	/**
	 * Constructor para un mensaje que tiene �nicamente su contenido cifrado con RSABloques (con la clave p�blica del receptor del mensaje).
	 * @param mensajeCifrado Mensaje con su contenido cifrado en RSABloques.
	 */
	public Mensaje(String mensajeCifrado) {
		this.mensajeCifrado = mensajeCifrado;
	}
	
	/**
	 * Comprueba si el cifrado utilizado es RSABloques para una clave de cifrado del algoritmo Hill.
	 * @return TRUE si es cifrado RSABloques para una clave de cifrado Hill, FALSE si se aplica otro cifrado.
	 */
	public boolean isHill() {
		boolean isHill = false;
		
		if (this.claveCifradaHill.size() > 0)
			isHill = true;
		
		return isHill;
	}
	
	/**
	 * Comprueba si el cifrado utilizado es RSABloques para una clave de cifrado del algoritmo Monoalfab�tico.
	 * @return TRUE si es cifrado RSABloques para una clave de cifrado Monoalfab�tico, FALSE si se aplica otro cifrado.
	 */
	public boolean isMonoalfabetico() {
		boolean isMonoalfabetico = false;
		
		if (this.claveCifradaMonoalfabetico.size() > 0)
			isMonoalfabetico = true;
		
		return isMonoalfabetico;
	}
	
	/**
	 * Comprueba si el cifrado utilizado es RSABloques para una clave de cifrado del algoritmo Vigen�re.
	 * @return TRUE si es cifrado RSABloques para una clave de cifrado Vigen�re, FALSE si se aplica otro cifrado.
	 */
	public boolean isVigenere() {
		boolean isVigenere = false;
		
		if (this.claveCifradaVigenere.size() > 0)
			isVigenere = true;
		
		return isVigenere;
	}
	
	/**
	 * Comprueba si el cifrado utilizado es RSABloques para una clave de cifrado del algoritmo C�sar.
	 * @return TRUE si es cifrado RSABloques para una clave de cifrado C�sar, FALSE si se aplica otro cifrado.
	 */
	public boolean isCesar() {
		boolean isCesar = false;
		
		if (this.claveCifradaCesar.size() > 0)
			isCesar = true;
		
		return isCesar;
	}
	
	/**
	 * Comprueba si el cifrado utilizado en el mensaje es �nicamente RSABloques para el cuerpo del mensaje.
	 * @return TRUE si solo es cifrado RSABloques para el mensaje, FALSE si se aplica otro cifrado.
	 */
	public boolean isSoloRSABloques() {
		boolean isSoloRSABloques = false;
		
		if (this.claveCifradaHill.size() == 0 && this.claveCifradaMonoalfabetico.size() == 0 && this.claveCifradaVigenere.size() == 0 && this.claveCifradaCesar.size() == 0)
			isSoloRSABloques = true;
		
		return isSoloRSABloques;
	}
}
