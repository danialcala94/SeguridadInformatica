package es.crab.algoritmos;

import java.math.BigInteger;
import java.util.ArrayList;

import es.crab.objetos.Usuario;
import es.crab.utiles.Utiles;

public class Descifrado {
	
	/**
	 * A partir de un mensajeCifrado, una clave p�blica compuesta por el par (n, e), un alfabeto en el que basarse y los primos p y q que dividen a n, devuelve
	 *  el mensaje original que se deber�a recibir si se ha cifrado con RSA en bloques.
	 * @param mensajeCifrado Mensaje cifrado con las claves p�blicas del receptor del mensaje.
	 * @param usuario Usuario receptor del mensaje (posee sus claves).
	 * @param alfabeto Alfabeto base utilizado para el mensaje.
	 * @return Mensaje original.
	 * @throws Exception 
	 * @see es.crab.algoritmos.Cifrado#RSABloques(String, BigInteger, BigInteger, String, BigInteger, BigInteger)
	 */
	public static String RSABloques(String mensajeCifrado, Usuario usuario, String alfabeto) {
		String mensajeDecodificado = "";
		
		BigInteger phi = Utiles.calcularPhi(usuario.getP(), usuario.getQ());
		
		// Calculamos la clave privada d
		BigInteger d = null;
		try {
			d = Utiles.algoritmoEuclidesExtendido(usuario.getE(), phi);
		} catch (Exception e1) {
			e1.printStackTrace();
			return "[ERROR: Excepci�n]";
		}
		
		// Calculamos k (longitud del bloque) y le sumamos 1 para el descifrado
		int k = Integer.parseInt(Utiles.log(new BigInteger(alfabeto.length() + ""), usuario.getN()).toString());
		
		// Dividimos el mensaje cifrado en palabras de tama�o [k + 1] (Rellenando con letra del alfabeto con valor 0 por la izquierda si hay cola)
		ArrayList<String> palabrasCifradas = Utiles.obtenerPalabras(mensajeCifrado, k + 1, alfabeto, false);

		// Obtenemos el valor num�rico de cada palabra cifrada
		ArrayList<BigInteger> valoresPalabrasCifradas = new ArrayList<BigInteger>();
		for (int i = 0; i < palabrasCifradas.size(); i++)
			valoresPalabrasCifradas.add(Utiles.valores2decimal(Utiles.palabra2valores(palabrasCifradas.get(i), alfabeto, 0), new BigInteger(alfabeto.length() + "")));
		
		// Calculamos el valor num�rico de las palabras originales
		ArrayList<BigInteger> valorPalabrasOriginales = new ArrayList<BigInteger>();
		for (int i = 0; i < valoresPalabrasCifradas.size(); i++)
			valorPalabrasOriginales.add(Utiles.algoritmoPotenciacionModular(valoresPalabrasCifradas.get(i), d, usuario.getN()));
		
		// Traducimos los valores num�ricos de las palabras originales a base N
		ArrayList<ArrayList<BigInteger>> valoresPalabrasOriginales = new ArrayList<ArrayList<BigInteger>>();
		for (int i = 0; i < valorPalabrasOriginales.size(); i++)
			valoresPalabrasOriginales.add(Utiles.obtenerExpresionEnBaseX(valorPalabrasOriginales.get(i), new BigInteger(alfabeto.length() + ""), k));
		
		for (int i = 0; i < valoresPalabrasOriginales.size(); i++)
			mensajeDecodificado += Utiles.valores2palabra(valoresPalabrasOriginales.get(i), new BigInteger(alfabeto.length() + ""), alfabeto, 0);
		
		return mensajeDecodificado;
	}
	
	/**
	 * Realiza un descifrado de un mensaje cifrado mediante una clave 2x2 y el m�todo Hill.
	 * @param mensajeCifrado Mensaje a descifrar.
	 * @param alfabeto Alfabeto base utilizado.
	 * @param a Primer elemento de la clave 2x2 utilizada.
	 * @param b Segundo elemento de la clave 2x2 utilizada.
	 * @param c Tercer elemento de la clave 2x2 utilizada.
	 * @param d Cuarto elemento de la clave 2x2 utilizada.
	 * @return Mensaje original.
	 * @throws Exception 
	 */
	public static String Hill(String mensajeCifrado, String alfabeto, ArrayList<BigInteger> arrayClave) {
		String mensajeDescifrado = "";
		int a = arrayClave.get(0).intValue();
		int b = arrayClave.get(1).intValue();
		int c = arrayClave.get(2).intValue();
		int d = arrayClave.get(3).intValue();
		
		//Para los casos en los que la clave tiene tamanho diferente a 2x2, no est� programado
		int [][] claveDescifrado= {{d, alfabeto.length()-b},{alfabeto.length()-c,a}};
		int numColumnas = claveDescifrado.length;
		int numFilas = mensajeCifrado.length()/numColumnas;
		int [][] matrizAux = new int[numFilas][numColumnas];
		int cont = 0;
		int [][] resultado = new int[matrizAux.length][claveDescifrado[0].length];
		
		//Calculamos el inverso del determinante
		int valorDet = (a*d)-(b*c);
		while(valorDet < 0){
			valorDet += alfabeto.length();
		}
		
		int det = 0;
		try {
			det = (Utiles.algoritmoEuclidesExtendido(BigInteger.valueOf(valorDet), BigInteger.valueOf(alfabeto.length()))).intValue();
		} catch (Exception e) {
			e.printStackTrace();
			return "[ERROR: Excepci�n]";
		}
		
		for(int i=0;i<claveDescifrado.length;i++){
			for(int j=0;j<claveDescifrado[0].length;j++){
				claveDescifrado[i][j] = (claveDescifrado[i][j]*det)%alfabeto.length();
			}
		}
		
		//Pasamos el mensaje a una matriz		
		for(int i=0;i<numFilas;i++){
			for(int j=0;j<numColumnas;j++){
				matrizAux[i][j] = alfabeto.indexOf(mensajeCifrado.charAt(cont));
				cont++;
			}
		}
		
		//Multiplicamos el mensaje por la clave de descifrado para calcular el mensajeDescifrado
		resultado = Utiles.multiplicacionMatrices(matrizAux, claveDescifrado);
		for(int i = 0; i<resultado.length;i++){
			for(int j=0;j<resultado[0].length;j++){
				mensajeDescifrado += alfabeto.charAt(resultado[i][j]%alfabeto.length());
			}
		}

		return mensajeDescifrado;
	}
	
	/**
	 * Descifra un mensaje cifrado mediante el m�todo monoalfab�tico y las claves a y b.
	 * @param a Primer elemento de la clave.
	 * @param b Segundo elemento de la clave.
	 * @param mensajeCifrado Mensaje cifrado con cifrado monoalfab�tico y clave a y b.
	 * @param alfabeto Alfabeto base utilizado.
	 * @return Mensaje original.
	 * @throws Exception 
	 */
	public static String Monoalfabetico(ArrayList<BigInteger> arrayClave, String mensajeCifrado, String alfabeto) {
		String mensajeDescifrado = "";
		BigInteger alfabetoLength = BigInteger.valueOf(alfabeto.length());
		
		//Creamos la clave de descifrado a partir de la clave de cifrado.
		BigInteger aDes = BigInteger.ZERO;
		try {
			aDes = Utiles.algoritmoEuclidesExtendido(arrayClave.get(0), alfabetoLength);
		} catch (Exception e) {
			e.printStackTrace();
			return "[ERROR: Excepci�n]";
		}
		if(aDes.compareTo(BigInteger.ZERO) == -1)
			aDes = alfabetoLength.add(aDes);
		
		BigInteger bDes = arrayClave.get(1).multiply((alfabetoLength.subtract(aDes)));
		if(bDes.compareTo(BigInteger.ZERO) == -1)
			bDes = alfabetoLength.add(bDes);
		
		for (int i = 0; i < mensajeCifrado.length(); i++)
			mensajeDescifrado += alfabeto.charAt((aDes.multiply(BigInteger.valueOf(alfabeto.indexOf(mensajeCifrado.charAt(i)))).add(bDes)).remainder(alfabetoLength).intValue());
		
		return mensajeDescifrado;
	}
	
	public static String Vigenere(ArrayList<BigInteger> arrayClave, String mensaje, String alfabeto){
		String mensajeDescifrado = "";
		int pos;
		int valor;
		
		for(int i=0;i<mensaje.length();i++){
			pos = i%(arrayClave.size());
			valor =  alfabeto.indexOf(mensaje.charAt(i)) - arrayClave.get(pos).intValue();
			
			if(valor<0)
				valor = alfabeto.length()+valor;
			
			mensajeDescifrado += alfabeto.charAt(valor);
			
		}
		return mensajeDescifrado;
	}
	
	public static String Cesar(String alfabeto, ArrayList<BigInteger> arrayClave, String mensaje) {
		String mensajeDescifrado = "";
		
		String finaly ="";
		int clave = arrayClave.get(0).intValue();
		int valor;
		//int pos = 0 ;
		String pos ="";
		int numeros ;
		for(int i = 0;i<mensaje.length();i++){
			valor = alfabeto.indexOf(mensaje.charAt(i));
			if(valor-clave <0) 									
				pos +=(alfabeto.length()+(valor-clave))+",";
			else 
				pos += (valor-clave)+",";
			
		}
		
		String posiciones[] = pos.split(",");
		for (int i = 0; i <posiciones.length; i++) {
			numeros = Integer.parseInt(posiciones[i]);
			mensajeDescifrado += alfabeto.charAt(numeros);
		}
		return mensajeDescifrado;
	}
	
	/**
	 * Dado un mensaje cifrado y un alfabeto, adem�s de la clave de cifrado (a, b), descifra el mensaje con cifrado monoalfab�tico (a * n + b)
	 * @param mensaje Mensaje a descifrarmadre .
	 * @param alfabeto Alfabeto utilizado.
	 * @param a Primer elemento de la clave.
	 * @param b Segundo elemento de la clave.
	 * @return Mensaje original.
	 * @throws Exception 
	 * @see es.crab.algoritmos.Cifrado#monoalfabetico(String, String, BigInteger, BigInteger)
	 */
	@Deprecated
	public static String monoalfab�tico(String mensajeCifrado, String alfabeto, BigInteger a, BigInteger b) {
		String mensajeDescifrado = "";
		
		BigInteger alfabetoLength = new BigInteger(alfabeto.length() + "");
		
		// Creamos la clave de descifrado a partir de la clave de cifrado
		BigInteger aDescifrado = null;
		try {
			aDescifrado = Utiles.algoritmoEuclidesExtendido(new BigInteger(a + ""),alfabetoLength);
		} catch (Exception e) {
			e.printStackTrace();
			return "[ERROR: Excepci�n]";
		}
		while (aDescifrado.compareTo(BigInteger.ZERO) == -1)
			aDescifrado = aDescifrado.add(alfabetoLength);
		
		BigInteger bDescifrado = b.multiply(alfabetoLength.add(aDescifrado));
		while (bDescifrado.compareTo(BigInteger.ZERO) == -1)
			bDescifrado = bDescifrado.add(alfabetoLength);
		
		for (int i = 0; i < mensajeCifrado.length(); i++)
			mensajeDescifrado += alfabeto.charAt((aDescifrado.multiply(new BigInteger(alfabeto.indexOf(mensajeCifrado.charAt(i)) + "")).add(bDescifrado).remainder(alfabetoLength)).intValue());
		
		return mensajeDescifrado;
	}
	
	

}
