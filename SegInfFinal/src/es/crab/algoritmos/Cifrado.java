package es.crab.algoritmos;

import java.math.BigInteger;
import java.util.ArrayList;

import es.crab.objetos.Usuario;
import es.crab.utiles.Utiles;

public class Cifrado {

	/**
	 * Realiza un cifrado RSA en bloques a partir de una clave p�blica compuesta por el par (n, e), un alfabeto en el que basarse y los primos p y q que dividen a n. Devuelve el mensaje
	 *  cifrado correspondiente.
	 * @param mensajeOriginal Mensaje a cifrar.
	 * @param usuario Usuario receptor del mensaje (incluye sus claves).
	 * @param alfabeto Alfabeto base utilizado para el mensaje.
	 * @return Mensaje cifrado.
	 * @see es.crab.algoritmos.Descifrado#RSABloques(String, BigInteger, BigInteger, String, BigInteger, BigInteger)
	 */
	public static String RSABloques(String mensajeOriginal, Usuario usuario, String alfabeto) {
		String mensajeCifrado = "";
		
		// Calculamos k (longitud del bloque) y le sumamos 1 para el descifrado
		int k = Integer.parseInt(Utiles.log(new BigInteger(alfabeto.length() + ""), usuario.getN()).toString());
		
		// Hace falta eliminar lo que se genera sobrante
		boolean eliminarSobrante = false;
		if (k % mensajeOriginal.length() == 0)
			eliminarSobrante = true;
		
		// Dividimos el mensaje cifrado en palabras de tama�o [k] (Rellenando con letra del alfabeto con valor 0 por la izquierda si hay cola)
		ArrayList<String> palabrasOriginales = Utiles.obtenerPalabras(mensajeOriginal, k, alfabeto, true);
		
		// Obtenemos el valor num�rico de cada palabra original
		ArrayList<BigInteger> valoresPalabrasOriginales = new ArrayList<BigInteger>();
		for (int i = 0; i < palabrasOriginales.size(); i++)
			valoresPalabrasOriginales.add(Utiles.valores2decimal(Utiles.palabra2valores(palabrasOriginales.get(i), alfabeto, 0), new BigInteger(alfabeto.length() + "")));
		
		// Calculamos el valor num�rico de las palabras cifradas
		ArrayList<BigInteger> valorPalabrasCifradas = new ArrayList<BigInteger>();
		for (int i = 0; i < valoresPalabrasOriginales.size(); i++)
			valorPalabrasCifradas.add(Utiles.algoritmoPotenciacionModular(valoresPalabrasOriginales.get(i), usuario.getE(), usuario.getN()));
		
		// Traducimos los valores num�ricos de las palabras originales a base N
		ArrayList<ArrayList<BigInteger>> valoresPalabrasCifradas = new ArrayList<ArrayList<BigInteger>>();
		for (int i = 0; i < valorPalabrasCifradas.size(); i++)
			valoresPalabrasCifradas.add(Utiles.obtenerExpresionEnBaseX(valorPalabrasCifradas.get(i), new BigInteger(alfabeto.length() + ""), k + 1));
		
		// Eliminamos lo sobrante si es que hac�a falta
		if (eliminarSobrante)
			valoresPalabrasCifradas.remove(valoresPalabrasCifradas.size() - 1);
		
		for (int i = 0; i < valoresPalabrasCifradas.size(); i++)
			mensajeCifrado += Utiles.valores2palabra(valoresPalabrasCifradas.get(i), new BigInteger(alfabeto.length() + ""), alfabeto, 0);
		
		return mensajeCifrado;
	}
	
	/**
	 * Realiza un cifrado mediante una clave 2x2 y el m�todo Hill.
	 * @param mensaje Mensaje a cifrar.
	 * @param alfabeto Alfabeto base utilizado.
	 * @param a Primer elemento de la clave 2x2 utilizada.
	 * @param b Segundo elemento de la clave 2x2 utilizada.
	 * @param c Tercer elemento de la clave 2x2 utilizada.
	 * @param d Cuarto elemento de la clave 2x2 utilizada.
	 * @return Mensaje cifrado con Hill.
	 */
	public static String Hill(String mensaje, String alfabeto, ArrayList<BigInteger> arrayClave) {
		String mensajeCifrado = "";
		int [][] clave = {{(arrayClave.get(0)).intValue(),(arrayClave.get(1)).intValue()},{(arrayClave.get(2)).intValue(),(arrayClave.get(3)).intValue()}};
		int numColumnas = clave.length;
		int cont = 0;
		int numFilas = mensaje.length()/numColumnas;
		int [][] matrizAux = new int[numFilas][numColumnas];
		int [][] matrizResultado = new int[matrizAux.length][clave[0].length];
		
		for(int i=0;i < numFilas;i++){
			for(int j=0; j < numColumnas;j++){
				matrizAux[i][j] = alfabeto.indexOf(mensaje.charAt(cont));
				cont++;
			}
		}
		System.out.println("!!!!!!!!!!!!!!!111");
		for(int i=0;i<matrizAux.length;i++){
			for(int j=0;j<matrizAux[0].length;j++){
				System.out.print(matrizAux[i][j]+"\t");
			}
			System.out.println();
		}
		
		matrizResultado = Utiles.multiplicacionMatrices(matrizAux, clave);
		System.out.println("!!!!!!!!!!!!!!!111");
		for(int i=0;i<matrizResultado.length;i++){
			for(int j=0;j<matrizResultado[0].length;j++){
				System.out.print(matrizResultado[i][j]+"\t");
			}
			System.out.println();
		}
		
		for(int i=0;i<matrizResultado.length;i++){
			for(int j=0;j<matrizResultado[0].length;j++){
				System.out.println(matrizResultado[i][j]%alfabeto.length());
				mensajeCifrado += alfabeto.charAt(matrizResultado[i][j]%alfabeto.length());
			}
		}
		
		return mensajeCifrado;
	}
	
	/**
	 * Cifra el mensaje dado mediante el m�todo monoalfab�tico y las claves a y b.
	 * @param a Primer elemento de la clave.
	 * @param b Segundo elemento de la clave.
	 * @param mensaje Mensaje a cifrar.
	 * @param alfabeto Alfabeto base utilizado.
	 * @return Mensaje cifrado con cifrado monoalfab�tico y clave a y b.
	 */
	public static String Monoalfabetico(ArrayList<BigInteger> arrayClave, String mensaje, String alfabeto) {
		String mensajeCifrado = "";
		BigInteger a = arrayClave.get(0);
		BigInteger b = arrayClave.get(1);
		int valor;
		
		// Calculamos el inverso de a en m�dulo b
		try {
			BigInteger inverso = Utiles.algoritmoEuclidesExtendido(a, b);
		} catch (Exception e) {
			e.printStackTrace();
			return "[ERROR: Excepci�n]";
		}
		
		for (int i = 0; i < mensaje.length(); i++)
			mensajeCifrado += alfabeto.charAt((a.multiply(BigInteger.valueOf(alfabeto.indexOf(mensaje.charAt(i)))).add(b)).remainder(BigInteger.valueOf(alfabeto.length())).intValue());
		
		return mensajeCifrado;
	}
	
	
	
	public static String Vigenere(ArrayList<BigInteger> arrayClave,String mensaje, String alfabeto){
		String mensajeCifrado = "";
		int pos;
		
		for(int i=0;i<mensaje.length();i++){
			pos = i%(arrayClave.size());
			mensajeCifrado += alfabeto.charAt((alfabeto.indexOf(mensaje.charAt(i)) + arrayClave.get(pos).intValue())%alfabeto.length());
		}
		
		return mensajeCifrado;
	}
	
	public static  String Cesar(String alfabeto, ArrayList<BigInteger> arrayClave, String mensajeClaro){
		String mensajeCifrado = "";
		String alfabetoAux = "";
		int posicion = arrayClave.get(0).intValue();
		int mod = alfabeto.length();
		int valor; 
		
		for(int i=0;i<mod;i++){
			valor = (posicion+i)%mod;
			
			if(valor < 0)
				valor = mod + valor;
			
			alfabetoAux += alfabeto.charAt(valor)+"";
		}
		for(int i = 0;i<mensajeClaro.length();i++){
			valor = alfabeto.indexOf(mensajeClaro.charAt(i));
			mensajeCifrado += alfabetoAux.charAt(valor);
		}
		
		return mensajeCifrado;
	}
	
	
	
	/**
	 * Dado un mensaje y un alfabeto, adem�s de la clave de cifrado (a, b), cifra el mensaje con cifrado monoalfab�tico (a * n + b)
	 * @param mensaje Mensaje a cifrar.
	 * @param alfabeto Alfabeto utilizado.
	 * @param a Primer elemento de la clave.
	 * @param b Segundo elemento de la clave.
	 * @return Mensaje cifrado con cifrado monoalfab�tico.
	 * @see es.crab.algoritmos.Descifrado#monoalfab�tico(String, String, BigInteger, BigInteger)
	 */
	@Deprecated
	public static String monoalfabetico(String mensaje, String alfabeto, BigInteger a, BigInteger b) {
		String mensajeCifrado = "";
		
		for (int i = 0; i < mensaje.length(); i++)
			mensajeCifrado += alfabeto.charAt((a.multiply(new BigInteger(alfabeto.indexOf(mensaje.charAt(i)) + "")).add(b).remainder(new BigInteger(alfabeto.length() + ""))).intValue());
		
		return mensajeCifrado;
	}
	
}
