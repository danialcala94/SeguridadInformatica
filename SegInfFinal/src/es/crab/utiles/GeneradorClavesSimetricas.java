package es.crab.utiles;

import java.math.BigInteger;
import java.util.ArrayList;

import com.mysql.fabric.xmlrpc.base.Array;

public class GeneradorClavesSimetricas {
	
	/**
	 * Genera una clave aleatoria para el cifrado de C�sar.
	 * @param alfabeto Alfabeto base utilizado.
	 * @return ArrayList de BigInteger con la clave. (<strong>a</strong> en la posici�n 0).
	 */
	public static ArrayList<BigInteger> generarClaveCesar(String alfabeto){
		ArrayList<BigInteger> clave = new ArrayList<BigInteger>();
		int valor = (int) (Math.random()*(alfabeto.length()));
		clave.add(BigInteger.valueOf(valor));
		return clave;
	}
	
	/**
	 * Genera una clave aleatoria para el cifrado de Vigen�re.
	 * @param alfabeto Alfabeto base utilizado.
	 * @param mensaje Mensaje para cifrar (utilizamos su longitud).
	 * @return ArrayList de BigInteger con claves en todas sus posiciones.
	 */
	public static ArrayList<BigInteger> generarClaveVigenere(String alfabeto, String mensaje){
		ArrayList<BigInteger> clave = new ArrayList<BigInteger>();
		int numCifras = 0;
		if(mensaje.length() == 1){
			numCifras = 1;
		}else if(mensaje.length() == 2){
			numCifras = (int) (Math.random()*2+1);
		}else{
			numCifras = (int) (Math.random()*(mensaje.length()/3))+1;
		}
		
		for(int i=0;i<numCifras;i++){
			int valor = (int) (Math.random()*alfabeto.length());
			clave.add(BigInteger.valueOf(valor));
		}
			
		return clave;
	}
	
	/**
	 * Genera una clave aleatoria para el cifrado de Monoalfab�tico [(a * mensaje) + b].
	 * @param alfabeto Alfabeto base utilizado.
	 * @return ArrayList de BigInteger con la clave. (<strong>a</strong> en la posici�n 0, <strong>b</strong> en la posici�n 1).
	 */
	public static ArrayList<BigInteger> generarClaveMonoalfabetico(String alfabeto){
		ArrayList<BigInteger> clave = null;
		boolean valido = false;
		while(!valido) {
			clave = new ArrayList<BigInteger>();
			for(int i=0;i<2;i++){
				int valor = (int)(Math.random()*alfabeto.length());
				clave.add(BigInteger.valueOf(valor));
			}
			if(Utiles.primosEntreSi(clave.get(0), new BigInteger(Integer.toString(alfabeto.length())))) {
				valido = true;
			}
		}
		return clave;
	}
	
	/**
	 * Genera una clave aleatoria para el cifrado de Hill [{{a, b}, {c, d}}].
	 * @param alfabeto Alfabeto base utilizado.
	 * @return ArrayList de BigInteger con la clave. (<strong>a</strong> en la posici�n 0, <strong>b</strong> en la posici�n 1, <strong>c</strong> en la posici�n 2, <strong>d</strong> en la posici�n 3).
	 */
	public static ArrayList<BigInteger> generarClaveHill(String alfabeto){
		boolean valido = false;
		ArrayList<BigInteger> claveHill = null;
		while(!valido) {
			claveHill = new ArrayList<BigInteger>();
			for(int i=0;i<4;i++){
				int valor = (int) (Math.random()*(alfabeto.length()));
				claveHill.add(BigInteger.valueOf(valor));
			}
			int valorDet = (claveHill.get(0).intValue()*claveHill.get(3).intValue())-(claveHill.get(1).intValue()*claveHill.get(2).intValue());
	
			while(valorDet < 0){
				valorDet += alfabeto.length();
			}
			if(Utiles.primosEntreSi(new BigInteger(Integer.toString(valorDet)), new BigInteger(Integer.toString(alfabeto.length())))) {
				valido = true;
			}
		}
		return claveHill;
	}
}
