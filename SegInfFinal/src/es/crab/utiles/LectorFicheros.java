package es.crab.utiles;

import java.io.BufferedReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LectorFicheros {

	/**
	 * Muestra el contenido del fichero por pantalla.
	 * @param fich
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void muestraContenido(String fich) throws FileNotFoundException, IOException {
        String cadena;
        
        FileReader fr = new FileReader(fich);
        BufferedReader b = new BufferedReader(fr);
        
        while ((cadena = b.readLine()) != null)
            System.out.println(cadena);
        
        b.close();
    }
	
	/**
	 * Obtiene el contenido del fichero.
	 * @param fich Ruta del fichero a leer.
	 * @return Contenido del fichero.
	 * @throws FileNotFoundException Si el fichero no existe.
	 * @throws IOException Si no se puede acceder al fichero.
	 */
	public static String leerContenido(String fich) throws FileNotFoundException, IOException {
		String cadena;
		String texto = "";
		
	    FileReader fr = new FileReader(fich);
	    BufferedReader b = new BufferedReader(fr);
	    
	    while ((cadena = b.readLine()) != null)
	        texto += cadena;
	    
	    b.close();
	    return texto;
	}
	
	/**
	 * Devuelve una lista con el contenido del fichero separado por el separador indicado.
	 * @param fich Ruta del fichero a leer.
	 * @param separador Separador del contenido
	 * @return Contenido del fichero en forma de lista.
	 * @throws FileNotFoundException Si el fichero no existe.
	 * @throws IOException Si no se puede acceder al fichero.
	 */
	public static ArrayList<BigInteger> leerLista(String fich, String separador) throws FileNotFoundException, IOException {
		String cadena;
		String texto = "";
		ArrayList<BigInteger> lista = new ArrayList<BigInteger>();
		
	    FileReader fr = new FileReader(fich);
	    BufferedReader b = new BufferedReader(fr);
	    
	    while ((cadena = b.readLine()) != null)
	        texto += cadena;
	    
	    StringTokenizer st = new StringTokenizer(texto,"-");
	    while (st.hasMoreTokens())
	    	lista.add(new BigInteger(st.nextToken()));
	    
	    b.close();
	    return lista;
	}
}
