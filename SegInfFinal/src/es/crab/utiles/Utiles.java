package es.crab.utiles;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

public class Utiles {
	
	/**
	 * Calcula si dos n�meros son primos entre s�, es decir, si el MCD(a, b) = 1.
	 * @param a Primer n�mero.
	 * @param b Segundo n�mero.
	 * @return TRUE si son primos entre s�, FALSE si no.
	 */
	public static boolean primosEntreSi(BigInteger a, BigInteger b) {
		BigInteger dividendo;
		BigInteger divisor;
		BigInteger cociente;
		BigInteger resto;
		
		boolean primosEntreSi = false;
		
		if (a.compareTo(b) > 0) {
			dividendo = a;
			divisor = b;
		} else {
			dividendo = b;
			divisor = a;
		}
		
		// Mientras que el divisor sea mayor que uno seguimos dividiendo
		while (divisor.compareTo(BigInteger.ONE) > 0) {
			cociente = dividendo.divide(divisor);
			resto = dividendo.remainder(divisor);
			dividendo = divisor;
			divisor = resto;
		}
		// Si el divisor es 0, el dividendo es el MCD. Si el divisor es 1, el MCD es 1.
		if (divisor.compareTo(BigInteger.ONE) == 0)
			primosEntreSi = true;
		
		return primosEntreSi;
	}
	
	/**
	 * Devuelve los n�meros primos existentes hasta el valor 2.000.000
	 * @return N�meros primos hasta 2.000.000.
	 */
	public static ArrayList<BigInteger> numerosPrimosHasta2Millones() {
		ArrayList<BigInteger> primos = new ArrayList<BigInteger>();
		
		try {
			primos = LectorFicheros.leerLista("recursos/primos.txt", "-");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return primos;
	}
	
	/**
	 * Devuelve un n�mero primo aleatorio entre 3 y 1.999.993, ambos incluidos.
	 * @return N�mero primo entre 3 y 1.999.993 al azar.
	 */
	public static BigInteger numeroPrimoAleatorio() {
		BigInteger primo = BigInteger.ONE;
		ArrayList<BigInteger> primos = new ArrayList<BigInteger>();
		
		primos = Utiles.numerosPrimosHasta2Millones();
		
		primo = primos.get((int) (Math.random() * primos.size()));
		
		return primo;
	}
	
	/**
	 * Devuelve la expresi�n decimal del valor quario expresado en q.
	 * @param quario Valor a convertir.
	 * @param q Base de representaci�n.
	 * @return Expresi�n en decimal.
	 */
	public static int quario2decimal(String quario, int q) {
	    int longitud = quario.length();
	    int valor = 0;

	    for (int i = (longitud - 1); i >= 0; i--)
	        valor += Integer.parseInt(quario.charAt(i) + "") * (Math.pow(q, longitud - 1 - i));

	    return valor;
	}
	
	/**
	 * Devuelve el valor decimal en expresado en quario.
	 * @param decimal N�mero decimal a representar.
	 * @param q Base de representaci�n.
	 * @return Expresi�n en quario.
	 */
	public static String decimal2quario(BigInteger decimal, BigInteger q) {
	    String quario = "";
	    BigInteger cociente = decimal;
	    BigInteger resto = BigInteger.ZERO;
	    boolean terminado = false;

	    while (!terminado) {
	        resto = decimal.remainder(q);
	        cociente = decimal.divide(q);
	        quario = resto + quario;
	        decimal = cociente;
	        if (cociente.compareTo(q) == -1) {
	            terminado = true;
	            quario = cociente + quario;
	        }
	    }

	    return quario;
	}
	
	/**
	 * Devuelve un array con los divisores del n�mero indicado.
	 * @param numero N�mero del que calcular los divisores.
	 * @return Divisores del n�mero.
	 */
	public static ArrayList<BigInteger> divisores(BigInteger numero) {
	    ArrayList<BigInteger> divisores = new ArrayList<BigInteger>();

	    for (BigInteger i = BigInteger.ONE; i.compareTo(numero.divide(new BigInteger("2")).add(BigInteger.ONE)) == -1; i = i.add(BigInteger.ONE)) {
	        if (numero.remainder(i).compareTo(BigInteger.ZERO) == 0)
	            divisores.add(i);
	    }

	    if (numero.compareTo(BigInteger.ONE) != 0)
	        divisores.add(numero);

	    return divisores;
	}

	/**
	 * Comprueba si el n�mero introducido es primo.
	 * @param numero N�mero que comprobar si es primo.
	 * @return TRUE si es primo, FALSE si no.
	 */
	public static boolean esPrimo(BigInteger numero) {
	    if (divisores(numero).size() <= 2)
	        return true;
	    else
	        return false;
	}

	/**
	 * Calcula los n�meros primos hasta el l�mite indicado.
	 * @param limiteSuperior L�mite hasta el que calcular n�meros primos.
	 * @return N�meros primos hasta el l�mite.
	 */
	public static ArrayList<BigInteger> primos(BigInteger limiteSuperior) {
	    ArrayList<BigInteger> primos = new ArrayList<BigInteger>();
	    
	    for (BigInteger cont = new BigInteger("2"); cont.compareTo(limiteSuperior.subtract(BigInteger.ONE)) <= 0; cont = cont.add(BigInteger.ONE)) {
	        if (esPrimo(cont))
	            primos.add(cont);
	    }

	    return primos;
	}

	/**
	 * Devuelve los n�meros primos divisores del n�mero dado.
	 * @param numero N�mero del que calcular los divisores.
	 * @return N�meros primos divisores del n�mero dado.
	 */
	public static ArrayList<BigInteger> primosDivisores(BigInteger numero) {
	    ArrayList<BigInteger> primos = new ArrayList<BigInteger>();
	    ArrayList<BigInteger> primosHastaNumero = primos(numero);
	    
	    for (int i = 0; i < primosHastaNumero.size(); i++) {
	        for (int j = 0; j < primosHastaNumero.size(); j++) {
	            if (primosHastaNumero.get(i).multiply(primosHastaNumero.get(j)).compareTo(numero) == 0) {
	                primos.add(primosHastaNumero.get(i));
	                primos.add(primosHastaNumero.get(j));
	                i = primosHastaNumero.size();
	                break;
	            }
	        }
	    }
	    
	    return primos;
	}
	
	/**
	 * A partir de la clave p�blica n, calcula el valor de phi.
	 * @param n Clave privada.
	 * @return Phi.
	 */
	public static BigInteger calcularPhi(BigInteger n) {
		ArrayList<BigInteger> primosDivisores = primosDivisores(n);
		
		BigInteger phi = primosDivisores.get(0).subtract(BigInteger.ONE).multiply(primosDivisores.get(1).subtract(BigInteger.ONE));
		
		return phi;
	}
	
	/**
	 * A partir de los dos primos (q y p) divisores de la clave privada n, calcula el valor de phi.
	 * @param p Primer n�mero primo divisor de n.
	 * @param q Segundo n�mero primo divisor de n.
	 * @return Phi.
	 */
	public static BigInteger calcularPhi(BigInteger p, BigInteger q) {
		p = p.subtract(BigInteger.ONE);
		q = q.subtract(BigInteger.ONE);
		
		BigInteger phi = p.multiply(q);
		
		return phi;
	}
	
	// ARRIBA VAN LAS NUEVAS Y M�S GEN�RICAS
	
	/**
	 * Crea la matriz generadora de la matriz <strong>A</strong> dada.
	 * @param A
	 * @return
	 */
	public static int[][] crearMatrizGeneradora(int[][] A) {
		int k = A.length;
		int[][] matGen = new int[k][k + A[0].length];
		for (int i = 0; i < k; i++) {
			for (int j = 0; j < (k + A[0].length); j++) {
				if (j < k) {
					if (j == i)
						matGen[i][j] = 1;
					else
						matGen[i][j] = 0;
				} else
					matGen[i][j] = A[i][j - k];
			}
		}
		return matGen;
	}
	
	/**
	 * Crea la matriz generadora de la matriz <strong>A</strong> dada.
	 * @param A
	 * @return
	 */
	public static int[][] crearMatrizGeneradora_IdentidadDerecha(int[][] A) {
		int k = A.length;
		int[][] matGen = new int[k][k + A[0].length];
		for (int i = 0; i < k; i++) {
			for (int j = 0; j < (k + A[0].length); j++) {
				if (j >= A[0].length) {
					if ((j - A[0].length) == i)
						matGen[i][j] = 1;
					else
						matGen[i][j] = 0;
				} else
					matGen[i][j] = A[i][j];
			}
		}
		return matGen;
	}
	
	/**
	 * Devuelve una nueva matriz con los <strong>longitud</strong> primeros elementos de cada fila original.
	 * @param matriz
	 * @param longitud
	 * @return
	 */
	public static int[][] dividirMatriz(int[][] matriz, int longitud) {
		int[][] matrizDividida = new int[matriz.length][longitud];
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < longitud; j++)
				matrizDividida[i][j] = matriz[i][j];
		}
		
		return matrizDividida;
	}
	
	/**
	 * Convierte un array en un String
	 * @param array
	 * @return
	 */
	public static String array2String(int[] array) {
		String valor = "";
		
		for (int i = 0; i < array.length; i++)
			valor += array[i];
		
		return valor;
	}
	
	/**
	 * Convierte un array en un String
	 * @param array
	 * @return
	 */
	public static String array2String(String[] array) {
		String valor = "";
		
		for (int i = 0; i < array.length; i++)
			valor += array[i];
		
		return valor;
	}
	
	/**
	 * Convierte un String en un array
	 * @param cadena
	 * @return
	 */
	public static int[] string2Array(String cadena) {
		int[] array = new int[cadena.length()];
		
		for (int i = 0; i < cadena.length(); i++)
			array[i] = cadena.charAt(i);
		
		return array;
	}
	
	/**
	 * Devuelve una matriz convertida a un array.
	 * @param matriz
	 * @return
	 */
	public static int[] matriz2Array(int[][] matriz) {
		int array[] = new int[matriz.length * matriz[0].length];
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++)
				array[(matriz[0].length * i) + j] = matriz[i][j];
		}
		
		return array;
	}
	
	/**
	 * Convierte un array en una matriz de <strong>longitud</strong> columnas. Si hay cola (resto), se pierde.
	 * @param array
	 * @param longitud
	 * @return
	 */
	public static int[][] array2Matriz(int[] array, int longitud) {
		int[][] matriz = new int[array.length / longitud][longitud];
		
		int numFila = 0; int cont = 0;
		for (int i = 0; i < array.length; i++) {
			matriz[numFila][cont] = array[i];
			cont++;
			if (cont == longitud) {
				numFila++;
				cont = 0;
			}
			if (numFila == matriz.length) {
				for (int j = i + 1; j < array.length; j++) {
					if (j == i + 1)
						System.out.println("LA COLA ES: ");
					System.out.println(array[j]);
				}
				break;
			}
		}
		
		return matriz;
	}
	
	/**
	 * Devuelve el n�mero de valores distintos de 0 de la fila pasada como par�metro.
	 * @param matriz
	 * @return
	 * @throws Exception 
	 */
	public static int distancia(String fila) throws Exception {
		if (fila.length() > 0) {
			int cont = 0;
			
			for (int i = 0; i < fila.length(); i++) {
				if (fila.charAt(i) != '0')
					cont++;
			}
			
			return cont;
		} else
			throw new Exception("[ERROR]: El String obtenido como par�metro est� vac�o.");
	}
	
	/**
	 * Devuelve el n�mero de valores distintos de 0 del array pasado como par�metro.
	 * @param matriz
	 * @return
	 * @throws Exception 
	 */
	public static int distancia(int[] fila) throws Exception {
		if (fila.length > 0) {
			int cont = 0;
			
			for (int i = 0; i < fila.length; i++) {
				if (fila[i] != 0)
					cont++;
			}
			
			return cont;
		} else
			throw new Exception("[ERROR]: El array obtenido como par�metro est� vac�o.");
	}
	
	/**
	 * Devuelve el n�mero de valores distintos entre un array (secuencia de valores) y otro pasados como par�metro.
	 * @param filaUno
	 * @param filaDos
	 * @return
	 * @throws Exception 
	 */
	public static int distanciaEntreDos(int[] filaUno, int[] filaDos) throws Exception {
		if (filaUno.length == filaDos.length) {
			int cont = 0;
			
			for (int i = 0; i < filaUno.length; i++) {
				if (filaUno[i] != filaDos[i])
					cont++;
			}
			
			return cont;
		} else
			throw new Exception("[ERROR]: Los arrays obtenidos como par�metros no tienen a la misma longitud.");
	}
	
	/**
	 * Devuelve la primera cadena de menor distancia de la matriz.
	 * @param matriz
	 * @return
	 */
	public static String menorDistancia_Cadena(int[][] matriz) {
		String resultado = "";
		
		int menorDist = Integer.MAX_VALUE;
		for (int i = 0; i < matriz.length; i++) {
			int dist =0;
			for (int j = 0; j < matriz[0].length; j++) {
				if (matriz[i][j] != 0){
					dist++;
				}				
			}
			if (dist < menorDist && dist != 0) {
				resultado = "";
				for (int x = 0; x < matriz[i].length; x++)
					resultado += matriz[i][x]; 
				menorDist = dist;
			}
		}
		
		return resultado;
	}
	
	/**
	 * Devuelve la longitud de primera cadena de menor distancia de la matriz.
	 * @param matriz
	 * @return
	 */
	public static int menorDistancia_Valor(int[][] matriz) {
		String cadena = menorDistancia_Cadena(matriz);
		int cont = 0;
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) != '0')
				cont++;
		}
		return cont;
	}
	
	/**
	 * Une las matrices introducidas por par�metros.
	 * @param matrizUno
	 * @param matrizDos
	 * @return
	 * @throws Exception
	 */
	public static int[][] unirMatrices(int[][] matrizUno, int[][] matrizDos) throws Exception {
		int[][] unida = new int[matrizUno.length][matrizUno[0].length + matrizDos[0].length];
		
		if (matrizUno.length == matrizDos.length) {
			for (int i = 0; i < matrizUno.length; i++) {
				for (int j = 0; j < (matrizUno[0].length + matrizDos[0].length); j++) {
					if (j < matrizUno[0].length)
						unida[i][j] = matrizUno[i][j];
					else
						unida[i][j] = matrizDos[i][j - matrizUno[0].length];
				}
			}
		} else
			throw new Exception("[ERROR]: Las matrices no tienen el mismo n�mero de filas.");
		
		return unida;
	}
	
	/**
	 * Devuelve la matriz identidad del <strong>tama�o</strong> indicado por par�metro.
	 * @param tamano
	 * @return
	 */
	public static int[][] generarIdentidad(int tamano) {
		int[][] identidad = new int[tamano][tamano];
		
		for (int i = 0; i < identidad.length; i++) {
			for (int j = 0; j < identidad[0].length; j++) {
				if (i == j)
					identidad[i][j] = 1;
				else
					identidad[i][j] = 0;
			}
		}
		
		return identidad;
	}
	
//	/**
//	 * Generar� la combinatoria para los q posibles s�mbolos, dada una longitud k, de forma ordenada. Si q = 2 y k = 5,
//	 * el primer valor ser� 00000 y el �ltimo 11111.
//	 * @param q
//	 * @param k
//	 * @return
//	 * @throws Exception 
//	 */
//	public static int[][] combinatoriaOptima(int q, int k) throws Exception {
//		if (q >= 2 && k >= 1) {
//			int filas = (int)Math.pow(q, k);
//			int[][] resultado = new int[filas][k];
//			
//			int numeroUnos = 0;
//			
//			for (int i = 0; i < filas; i++) {
//				for (int j = 0; j < k; j++) {
//						if (j < numeroUnos)
//							resultado[i][j] = 1;
//						else
//							resultado[i][j] = 0;
//					}
//					System.out.print(resultado[i][j]);
//				}
//				System.out.println();
//				cont = k - 1;
//				condicion = 0;
//			}
//			
//			return resultado;
//		} else
//			throw new Exception("[ERROR]: Ha introducido para q un valor menor que 2 o para k un valor menor que 1.");
//		
//	}

	/**
	 * Generar� la combinatoria para los q posibles s�mbolos, dada una longitud k.
	 * @param q
	 * @param k
	 * @return
	 */
	public static int[][] combinatoria_Int(int q, int k) {
		String[][] array = combinatoria_String(q, k);
		int[][] arrayInt = new int[array.length][array[0].length];
		
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[0].length; j++) {
				arrayInt[i][j] = Integer.parseInt(array[i][j]);
			}
		}
		
		return arrayInt;
	}
	
	/**
	 * Dada una <strong>fila</strong> de una matriz, la devuelve en forma de columna.
	 * @param fila
	 * @return
	 */
	public static int[][] convertirFilaEnColumna(int[] fila) {
		int[][] columna = new int[fila.length][1];
		
		for (int i = 0; i < fila.length; i++)
			columna[i][0] = fila[i];
		
		return columna;
	}
	
	/**
	 * Devuelve una la columna n�mero <strong>indiceColumna</strong> de la <strong>matriz</strong>
	 * @param matriz
	 * @param indiceColumna
	 * @return
	 */
	public static int[] convertirColumnaEnFila(int[][] matriz, int indiceColumna) {
		int[] fila = new int[matriz.length];
		
		for (int i = 0; i < fila.length; i++)
			fila[i] = matriz[i][indiceColumna];
		
		return fila;
	}
	
	/**
	 * Generar� la combinatoria para los q posibles s�mbolos, dada una longitud k.
	 * @param q
	 * @param k
	 * @return
	 */
	public static String[][] combinatoria_String(int q, int k) {
		int numElementos = (int) Math.pow(q, k);
		String[] arrayProvisional = new String[numElementos];
		int elementos = 0;
		while (elementos < numElementos) {
			String aleatorio = generarValorAleatorio(k, q);
			boolean yaEstaba = false;
			for (int i = 0; i < elementos; i++) {
				if (arrayProvisional[i].equals(aleatorio)) {
					yaEstaba = true;
					break;
				}
			}
			if (!yaEstaba) {
				arrayProvisional[elementos] = aleatorio;
				elementos++;
			}
		}
		
		String[][] arrayDefinitivo = new String[numElementos][k];
		for (int i = 0; i < numElementos; i++) {
			for (int j = 0; j < k; j++) {
				arrayDefinitivo[i][j] = arrayProvisional[i].charAt(j) + "";
			}
		}
		
		return arrayDefinitivo;
	}
	
	/**
	 * Genera un valor aleatorio en m�dulo <strong>q</strong> de <strong>k</strong> elementos.
	 * @param k
	 * @param q
	 * @return
	 */
	private static String generarValorAleatorio(int k, int q) {
		String valor = "";
		for (int i = 0; i < k; i++) {
			valor += aleatorio(q);
		}
		return valor;
	}
	
	/**
	 * Genera un valor aleatorio en m�dulo <strong>q</strong>
	 * @param q
	 * @return
	 */
	private static String aleatorio(int q) {
		int aleatorio = (int) (Math.random() * q);
		return Integer.toString(aleatorio);
	}
	
	/**
	 * Devuelve la matriz dada, en el m�dulo indicado como par�metro.
	 * @param matriz
	 * @param mod
	 * @return
	 */
	public static int[][] modX(int[][] matriz, int mod) {
		int matrizAux[][] = new int[matriz.length][matriz[0].length];
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++)
				matrizAux[i][j] = matriz[i][j] % mod;
		}
		
		return matrizAux;
	}
	
	/**
	 * Devuelve la matriz resultante de la multiplicaci�n de las dos matrices recibidas como par�metros.
	 * @param matrizA
	 * @param matrizB
	 * @return
	 */
	public static int[][] multiplicacionMatrices(int[][] matrizA, int[][] matrizB){
		int numFilas = matrizA.length;
		int numColumnas = matrizB[0].length;
		int columnasA = matrizA[0].length;
		int[][] resultado = new int[numFilas][numColumnas];
		
		for (int x = 0; x < resultado.length; x++) {
			for (int y = 0; y < resultado[x].length; y++) {
				for (int z = 0; z < columnasA; z++)
					resultado[x][y] += matrizA[x][z] * matrizB[z][y];
			}
		}
		
		return resultado;
	}
	
	/**
	 * Devuelve la matriz de control [ H = (-A^t | I(3)) ], dada la <strong>matrizA</strong> y su <strong>modulo</strong>.
	 * @param matrizA
	 * @param modulo
	 * @return
	 */
	public static int[][] calcularMatrizControl(int[][] matrizA, int modulo) {
		int[][] identidad = generarIdentidad(matrizA[0].length);
		int[][] invTra = inversa(trasponer(matrizA), modulo);
		int[][] matCon = null;
		try {
			matCon = unirMatrices(invTra, identidad);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		};
		return matCon;
	}
	
	
	/**
	 * Devuelve la traspuesta de la matriz obtenida como par�metro.
	 * @param matriz
	 * @return
	 */
	public static int[][] trasponer(int[][] matriz) {
		int[][] traspuesta = new int[matriz[0].length][matriz.length];
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++)
				traspuesta[j][i] = matriz[i][j];
		}
		
		return traspuesta;
	}
	
	/**
	 * Devuelve la inversa de la matriz obtenida como par�metro, en m�dulo <strong>mod</strong>. Si la matriz es A, devuelve -A.
	 * @param matriz
	 * @param mod
	 * @return
	 */
	public static int[][] inversa(int[][] matriz, int mod) {
		int[][] inversa = new int[matriz.length][matriz[0].length];
		
		matriz = modX(matriz, mod);
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				if (matriz[i][j] != 0)
					inversa[i][j] = mod - matriz[i][j];
			}
		}
		
		return inversa;
	}
	
	/**
	 * Muestra por pantalla la matriz obtenida como par�metro.
	 * @param matriz
	 */
	public static void mostrar(int[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++)
				System.out.print(matriz[i][j]);
			System.out.println();
		}
	}
	
	/**
	 * Dada una matriz del tipo String, devuelve la equivalente del tipo int.
	 * @param matriz
	 * @return
	 */
	public static int[][] matrizString2matrizInt(String[][] matriz) {
		int[][] matrizInt = new int[matriz.length][matriz[0].length];
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++)
				matrizInt[i][j] = Integer.parseInt(matriz[i][j]);
		}
		
		return matrizInt;
	}
	
	/**
	 * Devuelve un String con la <strong>matriz</strong> formateada para ser mostrada.
	 * @param matriz
	 * @return
	 */
	public static String matriz2String(String[][] matriz) {
		String resultado = "";
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++)
				resultado += matriz[i][j];
			resultado += "\n";
		}
		
		return resultado; 
	}
	
	/**
	 * Devuelve un String con la <strong>matriz</strong> formateada para ser mostrada.
	 * @param matriz
	 * @return
	 */
	public static String matriz2String(int[][] matriz) {
		String resultado = "";
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++)
				resultado += matriz[i][j];
			resultado += "\n";
		}
		
		return resultado;
	}
	
	/**
	 * Devuelve el valor del factorial de un <strong>n�mero</strong> dado.
	 * @param numero
	 * @return
	 */
	private static int factorial(int numero) {
		int num = numero;
		
		for (int i = numero - 1; i > 0; i--)
			num *= i;
		
		if (num != 0)
			return num;
		else
			return 1;
	}
	
	/**
	 * Devuelve el n�mero de opciones que hay de coger <strong>n</strong> elementos de <strong>y</strong> existentes, con orden, sin repeticiones.
	 * La f�rmula es: [ y! / (n! * (y - n)!) ]
	 * @param x
	 * @param y
	 * @return
	 * @throws Exception
	 */
	public static int opcionesCogerXdeY(int x, int y) throws Exception {
		if (x == y)
			return 1;
		else if (x > y)
			return 0;
		else if (x < 0 || y <= 0)
			throw new Exception("[ERROR]: Introduce valores negativos y una x menor o igual que y.");
		else
			return (factorial(y) / (factorial(x) * factorial(y - x)));
	}
	
	/**
	 * Determina el n�mero de opciones a generar en el mapa incompleto.
	 * @param n
	 * @param t
	 * @param modulo
	 * @return
	 */
	@Deprecated
	public static int calcularTamanoMapaIncompleto(int n, int t, int modulo) {
		int tamano = 0;
		
		for (int i = t; i >= 0; i--) {
			if (i == 0)
				tamano += 1;		// 000000
			else {
				try {
					tamano += (modulo - 1) * opcionesCogerXdeY(i, n);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return tamano;
	}
	
	/**
	 * (PREPARADO PARA APLICAR EL L�MITE SUPERIOR CON COMBINATORIA) Devuelve las opciones generadas de longitud <strong>n</strong>, con <strong>t</strong> elementos distintos de 0 
	 * y en el <strong>m�dulo</strong> indicado como par�metro
	 * @param n
	 * @param t
	 * @param modulo
	 * @return
	 */
	@Deprecated
	public static int[][] generarOpcionesCogerXdeY(int n, int t, int modulo) {
		int tamanoMatriz = 0;
		try {
			/*if (t == 0)
				tamanoMatriz = 0;
			else
				tamanoMatriz = (modulo - 1) * 2 * CodigosLineales.opcionesCogerXdeY(t, n);*/
			tamanoMatriz = 60;
			System.out.println("Generando: " + tamanoMatriz + " opciones...");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Una matriz con tantas filas como opciones, y tantas columnas como n (n�mero cifras)
		String[] opciones = new String[tamanoMatriz];
		int[] posiciones = new int[t];
		String valor = "";
		for (int i = 0; i < n; i++)
			valor += "0";
		int elementos = 0;
		
		while (elementos < tamanoMatriz) {
			int aleatoriosGenerados = 0;
			
			// Reiniciamos los valores
			posiciones = new int[t];
			valor = "";
			for (int i = 0; i < n; i++)
				valor += "0";
			
			// Generaremos t posiciones aleatorias para colocar en ellas valores aleatorios
			for (int i = 0; i < posiciones.length; i++) {
				boolean repetido = false;
				// (de 0 a (n-1))
				int aleatorio = (int) (Math.random() * n % n);
				// sin repetici�n
				for (int j = 0; j < aleatoriosGenerados; j++) {
					if (posiciones[j] == aleatorio) {
						repetido = true;
						break;
					}
				}
				if (repetido)
					i--;
				else {
					posiciones[i] = aleatorio;
					aleatoriosGenerados++;
				}
			}
			
			// DEBUG
			
			// Tenemos las posiciones aleatorias en las que generar n�meros aleatorios
			for (int i = 0; i < posiciones.length; i++) {
				// Generamos un aleatorio en el m�dulo correspondiente y distinto de 0
				String aleatorio = Integer.toString((int) (Math.random() * modulo % modulo));
				if (!aleatorio.equals("0")) {
					char[] tempCharArray = valor.toCharArray();
					tempCharArray[posiciones[i]] = aleatorio.charAt(0);
					valor = String.valueOf(tempCharArray);
				} else
					i--;
			}
			
			// Comprobar si ya estaba la secuencia aleatoria o si la podemos guardar
			boolean yaEstaba = false;
			for (int i = 0; i < elementos; i++) {
				if (opciones[i].equals(valor)) {
					yaEstaba = true;
					break;
				}
			}
			if (!yaEstaba) {
				opciones[elementos] = valor;
				elementos++;
			}
		}
		
		// Convertimos a tipo int para devolverlo
		int[][] opcionesInt = new int[opciones.length][opciones[0].length()];
		for (int i = 0; i < opciones.length; i++) {
			for (int j = 0; j < opciones[0].length(); j++)
				opcionesInt[i][j] = Integer.parseInt(opciones[i].charAt(j) + "");
		}
		
		return opcionesInt;
	}
	
	/**
	 * Devuelve las opciones generadas de longitud <strong>n</strong>, con desde 0 hasta <strong>t</strong> elementos distintos de 0 
	 * y en el <strong>m�dulo</strong> indicado como par�metro.
	 * @param n
	 * @param t
	 * @param modulo
	 * @return
	 */
	public static int[][] generarMapaIncompleto(int n, int t, int modulo) {
		// Una matriz con tantas filas como opciones, y tantas columnas como n (n�mero cifras)
		ArrayList<String> opciones = new ArrayList<String>();
		String valor = "";
		for (int i = 0; i < n; i++)
			valor += "0";
		int vecesIntentado = 0;
		final int VECESAINTENTAR = 100000;
		
		while (vecesIntentado < VECESAINTENTAR) {
			// Generamos de 0 a t aleatorios
			int aleatoriosAGenerar = (int) (Math.random() * t + 1);
			if (t == 0)
				aleatoriosAGenerar = 0;
			
			ArrayList<Integer> posiciones = new ArrayList<Integer>();
			
			// Reiniciamos los valores
			valor = "";
			for (int i = 0; i < n; i++)
				valor += "0";
			
			// Generaremos t posiciones aleatorias para colocar en ellas valores aleatorios
			for (int i = 0; i < aleatoriosAGenerar; i++) {
				boolean repetido = false;
				// (de 0 a (n-1))
				int aleatorio = (int) (Math.random() * n % n);
				// sin repetici�n
				for (int j = 0; j < posiciones.size(); j++) {
					if (posiciones.get(j) == aleatorio) {
						repetido = true;
						break;
					}
				}
				if (repetido)
					i--;
				else
					posiciones.add(aleatorio);
			}
			
			// DEBUG
			
			// Tenemos las posiciones aleatorias en las que generar n�meros aleatorios
			for (int i = 0; i < posiciones.size(); i++) {
				// Generamos un aleatorio en el m�dulo correspondiente y que puede ser 0
				String aleatorio = Integer.toString((int) (Math.random() * modulo % modulo));
				char[] tempCharArray = valor.toCharArray();
				tempCharArray[posiciones.get(i)] = aleatorio.charAt(0);
				valor = String.valueOf(tempCharArray);
			}
			
			// Comprobar si ya estaba la secuencia aleatoria o si la podemos guardar
			boolean yaEstaba = false;
			for (int i = 0; i < opciones.size(); i++) {
				if (opciones.get(i).equals(valor)) {
					yaEstaba = true;
					break;
				}
			}
			if (!yaEstaba) {
				opciones.add(valor);
				vecesIntentado = 0;
			} else
				vecesIntentado++;
		}
		
		// Convertimos a tipo int para devolverlo
		int[][] opcionesInt = new int[opciones.size()][opciones.get(0).length()];
		for (int i = 0; i < opciones.size(); i++) {
			for (int j = 0; j < opciones.get(0).length(); j++)
				opcionesInt[i][j] = Integer.parseInt(opciones.get(i).charAt(j) + "");
		}
		
		return opcionesInt;
	}
	
	/**
	 * Divide el mensaje dado, en una matriz con tantas filas como palabras.
	 * @param mensaje
	 * @param longitudPalabra
	 * @return
	 */
	public static int[][] dividirMensaje(int[] mensaje, int longitudPalabra) {
		int[][] mensajeDividido = new int[mensaje.length / longitudPalabra][longitudPalabra];
		
		for (int i = 0; i < mensaje.length / longitudPalabra; i++) {
			for (int j = 0; j < longitudPalabra; j++)
				mensajeDividido[i][j] = mensaje[(longitudPalabra * i) + j];
		}
		
		return mensajeDividido;
	}
	
	/**
	 * Calcula la cola restante de un mensaje. (Si no hay cola => .length == 0)
	 * @param mensaje
	 * @param longitudPalabra
	 * @return
	 */
	public static int[] calcularCola(int[] mensaje, int longitudPalabra) {
		int[] cola = new int[mensaje.length % longitudPalabra];
		
		int indiceInicio = longitudPalabra * (mensaje.length / longitudPalabra);
		int cont = 0;
		for (int i = indiceInicio; i < mensaje.length; i++) {
			cola[cont] = mensaje[i];
			cont++;
		}
		
		return cola;
	}
	
	
	
	
	/**
	 * Calcular la distancia de Hamming a partir de una <strong>matriz</strong> inicial y una <strong>q</strong> dada.
	 * @param matriz
	 * @param q
	 * @return
	 */
	public static String calcularDistanciaHamming(int[][] matriz, int q) {
		String distanciaHamming = "";
		
		// Creamos la matriz generadora de la matriz dada
		int matrizGeneradora[][] = crearMatrizGeneradora(matriz);
		// k es el n�mero de filas
		int k = matriz.length;
		// Calculamos las posibilidades q^k
		int[][] combinatoria = matrizString2matrizInt(combinatoria_String(q, k));
		// Multiplicamos toda la combinatoria por la matriz generadora
		int multiplicacion[][] = modX(multiplicacionMatrices(combinatoria, matrizGeneradora), q);
		// Obtenemos la (primera) cadena de menor distancia
		distanciaHamming = menorDistancia_Cadena(multiplicacion);
		
		return distanciaHamming;
	}
	
	/**
	 * Divide el mensaje en tantas palabras de tama�o k como haya en el mensaje.
	 * @param mensaje Mensaje a dividir en palabras.
	 * @param k Tama�o del bloque.
	 * @return Palabras en el mensaje de longitud k.
	 */
	public static ArrayList<String> obtenerPalabras(String mensaje, int k, String alfabeto, boolean mensajeOriginal) {
		ArrayList<String> palabras = new ArrayList<String>();
		
		int numeroPalabras = mensaje.length() / k;
		int restoCaracteres = mensaje.length() - (numeroPalabras * k);
		int ultimoIndice = numeroPalabras * k - 1;
		for (int i = 0; i < numeroPalabras; i++) {
			String palabra = "";
			for (int j = 0; j < k; j++)
				palabra += mensaje.charAt((i * k) + j);
			palabras.add(palabra);
		}
		
		String palabra = "";
		// Si estamos leyendo del mensaje cifrado, debemos rellenar con caracteres con valor cero a la izquierda.
		if (!mensajeOriginal) {
			for (int i = ultimoIndice; i < mensaje.length() - 1; i++) {
				for (int j = 0; j < k - (mensaje.length() - ultimoIndice); j++) {
					// Si la primera letra del alfabeto no tiene valor 0 habr�a que modificar esto
					palabra += alfabeto.charAt(0);
				}
				palabra += mensaje.charAt(i);
			}
		// Si estamos leyendo del mensaje original, debemos rellenar con espacios al final.
		} else {
			for (int i = 0; i < k; i++) {
				if (i < restoCaracteres)
					palabra += mensaje.charAt(numeroPalabras * k + i);
				else
					palabra += " ";
			}
		}
		
		if (palabra.length() > 0)
			palabras.add(palabra);
		
		return palabras;
	}
	
	/**
	 * Devuelve los valores decimales de los caracteres presentes en la palabra indicada, con respecto al alfabeto dado.
	 * @param palabra Palabra de la que obtener el valor de los caracteres.
	 * @param alfabeto Alfabeto del que reconocer los caracteres.
	 * @param valorPosicionCero Valor decimal de la letra en primera posici�n del alfabeto.
	 * @return Valores decimales de los caracteres de la palabra.
	 */
	public static ArrayList<Integer> palabra2valores(String palabra, String alfabeto, int valorPosicionCero) {
		ArrayList<Integer> valores = new ArrayList<Integer>();
		
		for (int i = 0; i < palabra.length(); i++) {
			for (int j = 0; j < alfabeto.length(); j++) {
				if (palabra.charAt(i) == alfabeto.charAt(j)) {
					if (j + valorPosicionCero >= alfabeto.length())
						valores.add((valorPosicionCero + j) % alfabeto.length());
					else
						valores.add(valorPosicionCero + j);
					break;
				}
			}
		}
		
		return valores;
	}
	
	/**
	 * A partir de los valores de una determinada palabra, devuelve la palabra correspondiente.
	 * @param valores Valores de las letras de la palabra.
	 * @param base Base de representaci�n.
	 * @param alfabeto Alfabeto correspondiente a las letras de la palabra.
	 * @param valorPosicionCero Valor decimal de la priemra letra del alfabeto.
	 * @return
	 */
	public static String valores2palabra(ArrayList<BigInteger> valores, BigInteger base, String alfabeto, int valorPosicionCero) {
	    String palabra = "";
	    
	    for (int i = 0; i < valores.size(); i++) {
	        int posicion = Integer.parseInt(valores.get(i).subtract(new BigInteger(valorPosicionCero + "")).toString());
	        if (posicion < 0)
	            posicion = alfabeto.length() + posicion;
	        palabra += alfabeto.charAt(posicion);
	    }
	    
	    return palabra;
	}
	
	/**
	 * Devuelve el valor decimal calculado a partir de los valores y la base dadas. [20, 3, 5] (base 7) = 20 * 7^2 + 3 * 7^1 + 5 * 7^0 = 1008.
	 * @param valores Valores decimales de las letras de una determinada palabra.
	 * @param base Base de representaci�n.
	 * @return El valor decimal correspondiente.
	 */
	public static BigInteger valores2decimal(ArrayList<Integer> valores, BigInteger base) {
		BigInteger valorDecimal = BigInteger.ZERO;
		
		for (int i = valores.size() - 1; i >= 0; i--)
	        valorDecimal = valorDecimal.add(base.pow(valores.size() - 1 - i).multiply(new BigInteger(valores.get(i).toString())));
		
		return valorDecimal;
	}
	
	// OTRO
	
	/**
	 * /**
	 * Devuelve el inverso del valor en el m�dulo indicado, mediante el m�todo del Algoritmo de Euclides Extendido.
	 * @param valor Valor del que calcular el inverso.
	 * @param modulo M�dulo a aplicar.
	 * @return Inverso del valor en el m�dulo indicado.
	 * @throws Exception Si el MCD es distinto de 1.
	 */
	public static BigInteger algoritmoEuclidesExtendido(BigInteger valor, BigInteger modulo) throws Exception {
		if (!Utiles.primosEntreSi(valor, modulo))
			throw new Exception("Para calcular el Algoritmo de Euclides Extendido, el M�ximo Com�n Divisor de los dos valores debe ser 1, y no lo es.");
		
		ArrayList<BigInteger> cocientes = new ArrayList<BigInteger>();
		ArrayList<BigInteger> restos = new ArrayList<BigInteger>();
		ArrayList<BigInteger> mus = new ArrayList<BigInteger>();
		
		// Paso inicial (crear las dos primeras filas)
		cocientes.add(new BigInteger("0"));
		cocientes.add(new BigInteger("0"));
		restos.add(modulo);
		restos.add(valor);
		mus.add(new BigInteger("0"));
		mus.add(new BigInteger("1"));
		
		// Si el valor nos lo dan negativo lo convertimos en positivo.
		if (valor.compareTo(new BigInteger("0")) == -1) {
			while (valor.compareTo(new BigInteger("0")) < 0)
				valor = valor.add(modulo);
		}
		
		// Si el valor indicado es mayor que el m�dulo, lo hacemos menor
		if (valor.compareTo(modulo) == 1)
			valor = valor.remainder(modulo);
		
		// Comenzar algoritmo
		while (restos.get(restos.size() - 1).compareTo(new BigInteger("1")) != 0) {
			BigInteger valorCocientes = restos.get(restos.size() - 2).divide(restos.get(restos.size() - 1));
			BigInteger valorRestos = restos.get(restos.size() - 2).remainder(restos.get(restos.size() - 1));
			cocientes.add(valorCocientes);
			restos.add(valorRestos);
			BigInteger valorMus = mus.get(mus.size() - 2).subtract(cocientes.get(cocientes.size() - 1).multiply(mus.get(mus.size() - 1)));
			mus.add(valorMus);
		}
		
		if (mus.get(mus.size() - 1).compareTo(new BigInteger("0")) == -1) {
			while (mus.get(mus.size() - 1).compareTo(new BigInteger("0")) < 0)
				mus.set(mus.size() - 1, mus.get(mus.size() - 1).add(modulo));
		}
		
		return mus.get(mus.size() - 1);
	}
	
	/**
	 * Devuelve el M�ximo Com�n Divisor (MCD) de los dos valores indicados.
	 * @param a Primer valor.
	 * @param b Segundo valor.
	 * @return M�ximo Com�n Divisor de a y b.
	 */
	public static BigInteger maximoComunDivisor(BigInteger a, BigInteger b) {
		ArrayList<BigInteger> divisoresA = Utiles.divisores(a);
		ArrayList<BigInteger> divisoresB = Utiles.divisores(b);
		BigInteger mcd = BigInteger.ONE;
		
		for (int i = 0; i < divisoresA.size(); i++) {
			for (int j = 0; j < divisoresB.size(); j++) {
				if (divisoresA.get(i).compareTo(divisoresB.get(j)) == 0)
					mcd = divisoresA.get(i);
			}
		}
		
		return mcd;
	}
	
	/**
	 * 
	 * @param base
	 * @param potencia
	 * @param modulo
	 * @return
	 */
	public static BigInteger algoritmoPotenciacionModular(BigInteger base, BigInteger potencia, BigInteger modulo) {
		String potenciaBinario = Utiles.convertirABinario(potencia);
		
		ArrayList<BigInteger> a = new ArrayList<BigInteger>();
		ArrayList<BigInteger> b = new ArrayList<BigInteger>();
		ArrayList<BigInteger> m = new ArrayList<BigInteger>();
		
		// Rellenamos en b el binario, de derecha a izquierda
		for (int i = potenciaBinario.length() - 1; i >= 0; i--)
			b.add(new BigInteger(potenciaBinario.charAt(i) + ""));
		
		// Rellenamos a
		a.add(base);
		for (int i = 0; i < b.size() - 1; i++)
			a.add(a.get(a.size() - 1).multiply(a.get(a.size() - 1)).mod(modulo));
		
		// Rellenamos m
		m.add(new BigInteger("1"));
		for (int i = 0; i < b.size(); i++) {
			if (b.get(i).compareTo(new BigInteger("1")) == 0)
				m.add((a.get(i).multiply(m.get(i)).mod(modulo)));
			else
				m.add(m.get(i));
		}
		
		return m.get(m.size() - 1);
	}
	
	/**
	 * Devuelve la <strong>expresi�n</strong> dada, en la <strong>base</strong> indicada.
	 * @param expresion
	 * @param base
	 * @return
	 */
	public static ArrayList<BigInteger> obtenerExpresionEnBaseX(BigInteger expresion, BigInteger base, int k) {
		ArrayList<BigInteger> resultado = new ArrayList<BigInteger>();
		
		BigInteger resto = BigInteger.ZERO;
		BigInteger cociente = BigInteger.ZERO;
		
		while (expresion.compareTo(base) >= 0) {
			cociente = expresion.divide(base);
			resto = expresion.remainder(base);
			resultado.add(resto);
			expresion = cociente;
		}
		
		resultado.add(cociente);
		
		// Reordenamos el array
		ArrayList<BigInteger> resultadoOrdenado = new ArrayList<BigInteger>();
		for (int i = resultado.size() - 1; i >= 0; i--)
			resultadoOrdenado.add(resultado.get(i));
		
		// Si la palabra no tiene el tama�o esperado, rellenamos con ceros por la izquierda
		while (resultadoOrdenado.size() < k)
			resultadoOrdenado.add(0, BigInteger.ZERO);
		
		return resultadoOrdenado;
	}
	
	/**
	 * Devuelve los divisores del <strong>numero</strong> pasado como par�metro.
	 * @param numero
	 * @return
	 */
	public static ArrayList<BigInteger> obtenerDivisores(BigInteger numero) {
		ArrayList<BigInteger> divisores = new ArrayList<BigInteger>();
		
		for (BigInteger i = BigInteger.valueOf(1); i.compareTo(numero) < 0; i = i.add(BigInteger.ONE)) {
			if (numero.remainder(i) == BigInteger.ZERO)
				divisores.add(new BigInteger(i + ""));
		}
		
		return divisores;
	}
	
	/**
	 * Devuelve el logaritmo en la <strong>base</strong> de n.
	 * @param base
	 * @param n
	 * @return
	 */
	public static BigInteger log(BigInteger base, BigInteger n) {
		boolean encontrado = false;
		BigInteger valor = new BigInteger("0");
		int cont = 1;
		
		while (!encontrado) {
			if (base.pow(cont).compareTo(n) > 0) {
				valor = new BigInteger(cont - 1 + "");
				encontrado = true;
			}
			cont++;
		}
		
		return valor;
	}
	
	// OTRO
	
	public static int binarioADecimal(String binario) {
		int longitud = binario.length();
		int valor = 0;
		for (int i = (longitud - 1); i >= 0; i--) {
			valor += Integer.parseInt(binario.charAt(i) + "") * (Math.pow(2, longitud - 1 - i));
		}
		return valor;
	}
	
	public static int ternarioADecimal(String ternario) {
		int longitud = ternario.length();
		int valor = 0;
		for (int i = (longitud - 1); i >= 0; i--) {
			valor += Integer.parseInt(ternario.charAt(i) + "") * (Math.pow(3, longitud - 1 - i));
		}
		
		return valor;
	}
	
	/**
	 * Devuelve el <strong>numero</strong> obtenido como par�metro, en binario, con una longitud <strong>longitud</strong>
	 * @param numero
	 * @param longitud
	 * @return
	 */
	public static String convertirABinario(int numero, int longitud) {
		String binario = convertirABinario(numero);
		for (int i = binario.length(); i < longitud; i++)
			binario = "0" + binario;
		return binario;
	}
	
	/**
	 * Devuelve el <strong>numero</strong> obtenido como par�metro, en binario.
	 * @param numero
	 * @return
	 */
	public static String convertirABinario(int numero) {
		String binario = "";
		int cociente = numero;
		int resto = 0;
		boolean terminado = false;
		while (!terminado) {
			resto = numero % 2;
			cociente = numero / 2;
			binario = resto + binario;
			numero = cociente;
			if (cociente < 2) {
				terminado = true;
				binario = cociente + binario;
			}
		}
		return binario;
	}
	
	/**
	 * Devuelve el <strong>numero</strong> obtenido como par�metro, en binario, partiendo de un valor BigInteger.
	 * @param numero
	 * @return
	 */
	public static String convertirABinario(BigInteger numero) {
		String binario = "";
		BigInteger cociente = numero;
		BigInteger resto = new BigInteger("0");
		boolean terminado = false;
		while (!terminado) {
			resto = numero.remainder(new BigInteger("2"));
			cociente = numero.divide(new BigInteger("2"));
			binario = resto.toString() + binario;
			numero = cociente;
			if (cociente.compareTo(new BigInteger("2")) == -1) {
				terminado = true;
				binario = cociente.toString() + binario;
			}
		}
		return binario;
	}
	
	/**
	 * A partir de un <strong>String textoLeidoDeFichero</strong> dado, se obtienen las frecuencias separadas por el <strong>String separador</strong> indicado, habiendo eliminado previamente los
	 * elementos inservibles del string incluidos en el <strong>ArrayList<String> elementosAEliminar</strong>.
	 * @param textoLeidoDeFichero
	 * @param elementosAEliminar
	 * @param separador
	 * @return
	 */
	public static ArrayList<Integer> obtenerFrecuenciasDeString(String textoLeidoDeFichero, ArrayList<String> elementosAEliminar, String separador) {
		for (int i = 0; i < elementosAEliminar.size(); i++)
			textoLeidoDeFichero = textoLeidoDeFichero.replace(elementosAEliminar.get(i), "");
		String array[] = textoLeidoDeFichero.split(separador);
		
		ArrayList<Integer> frecuenciasLeidas = new ArrayList<Integer>();
		for (int i = 0; i < array.length; i++)
			frecuenciasLeidas.add(Integer.parseInt(array[i]));
		
		return frecuenciasLeidas;
	}
	
	/**
	 * A partir de un <strong>String textoLeidoDeFichero</strong> dado, se obtienen las frecuencias separadas por el <strong>String separador</strong> indicado, habiendo eliminado previamente los
	 * elementos inservibles del string incluidos en el <strong>String[] elementosAEliminar</strong>.
	 * @param textoLeidoDeFichero
	 * @param elementosAEliminar
	 * @param separador
	 * @return
	 */
	public static ArrayList<Integer> obtenerFrecuenciasDeString(String textoLeidoDeFichero, String[] elementosAEliminar, String separador) {
		for (int i = 0; i < elementosAEliminar.length; i++)
			textoLeidoDeFichero = textoLeidoDeFichero.replace(elementosAEliminar[i], "");
		String array[] = textoLeidoDeFichero.split(separador);
		
		ArrayList<Integer> frecuenciasLeidas = new ArrayList<Integer>();
		for (int i = 0; i < array.length; i++)
			frecuenciasLeidas.add(Integer.parseInt(array[i]));
		
		return frecuenciasLeidas;
	}
	
	/**
	 * A partir de un <strong>String textoLeidoDeFichero</strong> dado, se obtienen las probabilidades separadas por el <strong>String separador</strong> indicado, habiendo eliminado previamente los
	 * elementos inservibles del string incluidos en el <strong>ArrayList<String> elementosAEliminar</strong>.
	 * @param textoLeidoDeFichero
	 * @param elementosAEliminar
	 * @param separador
	 * @return
	 */
	public static ArrayList<Double> obtenerProbabilidadesDeString(String textoLeidoDeFichero, ArrayList<String> elementosAEliminar, String separador) {
		for (int i = 0; i < elementosAEliminar.size(); i++)
			textoLeidoDeFichero = textoLeidoDeFichero.replace(elementosAEliminar.get(i), "");
		String array[] = textoLeidoDeFichero.split(separador);
		
		ArrayList<Double> probabilidadesLeidas = new ArrayList<Double>();
		for (int i = 0; i < array.length; i++)
			probabilidadesLeidas.add(Double.parseDouble(array[i]));
		
		return probabilidadesLeidas;
	}
	
	/**
	 * A partir de un <strong>String textoLeidoDeFichero</strong> dado, se obtienen las probabilidades separadas por el <strong>String separador</strong> indicado, habiendo eliminado previamente los
	 * elementos inservibles del string incluidos en el <strong>String[] elementosAEliminar</strong>.
	 * @param textoLeidoDeFichero
	 * @param elementosAEliminar
	 * @param separador
	 * @return
	 */
	public static ArrayList<Double> obtenerProbabilidadesDeString(String textoLeidoDeFichero, String[] elementosAEliminar, String separador) {
		for (int i = 0; i < elementosAEliminar.length; i++)
			textoLeidoDeFichero = textoLeidoDeFichero.replace(elementosAEliminar[i], "");
		String array[] = textoLeidoDeFichero.split(separador);
		
		ArrayList<Double> probabilidadesLeidas = new ArrayList<Double>();
		for (int i = 0; i < array.length; i++)
			probabilidadesLeidas.add(Double.parseDouble(array[i]));
		
		return probabilidadesLeidas;
	}
	
}
