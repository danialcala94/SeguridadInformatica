package es.crab.aplicacion;

import java.math.BigInteger;
import java.util.ArrayList;

import es.crab.algoritmos.Cifrado;
import es.crab.algoritmos.Descifrado;
import es.crab.objetos.Usuario;
import es.crab.persistencia.BaseDeDatos;
import es.crab.utiles.Utiles;

public class Aplicacion {
	
	static private ArrayList<BigInteger> primos = new ArrayList<BigInteger>();
	static private ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();
	static private String alfabeto = "abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ����������0123456789 .,:;-_()!��?{}[]<>*";
	static public final int  HILL = 1;
	static public final int MONOALFABETICO = 2;
	static public final int VIGENERE = 3;
	static public final int CESAR = 4;
	static public final String PREFIJO = "[Cifrado CRAB]";
	
	
	/**
	 * Devuelve los n�meros primos generados en la aplicaci�n.
	 * @return N�meros primos de 3 a 1.999.993 (ambos incluidos).
	 */
	public static ArrayList<BigInteger> getPrimos() {
		return Aplicacion.primos;
	}
	
	public static void iniciaAplicacion() {
		// Calculamos los primos al ejecutar la aplicaci�n para tenerlos ya en memoria y poder utilizarlos
		primos = Utiles.numerosPrimosHasta2Millones();
	}

	/*
	public static void pruebas() throws Exception {	
		// Calculamos los primos al ejecutar la aplicaci�n para tenerlos ya en memoria y poder utilizarlos
		primos = Utiles.numerosPrimosHasta2Millones();�
		
		System.out.println("Listando usuarios en la base de datos:");
		ArrayList<Usuario> usuarios = BaseDeDatos.getUsuarios();
		for (int i = 0; i < usuarios.size(); i++)
			System.out.println(usuarios.get(i).toString());
		System.out.println();
		
		
		String alfabeto = "abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ����������0123456789 ,.:;-�?()";
		String alfEx = "ABCDEFGHIJKLMN�OPQRSTUVWXYZ";
		
		// Cifrado monoalfab�tico
		ArrayList<BigInteger> claves = new ArrayList<BigInteger>();
		claves.add(new BigInteger("8849169404252643679"));
		claves.add(new BigInteger("356812573"));
		System.out.println("Mono01: " + Cifrado.Monoalfabetico(claves, "Esto es un mensaje de prueba.", alfabeto));
		// Descifrado monoalfab�tico
		System.out.println("Mono01: " + Descifrado.Monoalfabetico(new BigInteger("8849169404252643679"), new BigInteger("356812573"), "��jvdq�dNQdnqQ��eqd6qdZENqi�1", alfabeto));
		
		
		// PRUEBAS BEA
		
		String alfabetoHill = "0123456789abcdefghijklmn�opqrstuvwxyz";
		alfabeto = "abcdefghijklmn�opqrstuvwxyz0123456789 ";
		String alfabeto2 = "abcdefghijklmn�opqrstuvwxyz";
		String alfabeto3 = "abcdefghijklmn�opqrstuvwxyz ";
		System.out.println(alfabetoHill.length());
		//PRUEBAS PARA CIFRADO C�SAR
		int posicion = 2;
		String mensaje = "hola peladilo";
		System.out.println("El mensaje ----> "+ mensaje + " cifrado mediante el cifrado cesar  con una "
				+ "posicion igual a "+ posicion+ " da el siguiente mensaje " +Cifrado.Cesar(alfabeto, posicion, mensaje));
		
		System.out.println("---------------------------------!!!!!!-----------------------------------\n");
		//PRUEBAS PARA CIFRADO VIGEN�RE
		String mensaje2 = "marinita9";
		String clave = "bcd";
		System.out.println("El mensaje "+mensaje2+" con la clave de cifrado "+clave+" dentro de cifrado vignere"
				+ "da como mensaje cifrado "+Cifrado.Vigenere(clave, mensaje2, alfabeto));
		
		//PRUEBAS PARA DESCIFRADO VIGEN�RE
		String mensaje3 = "ncujolucb";
		String clave2 = "bcd";
		System.out.println("El mensaje "+mensaje3+" con la clave de cifrado "+clave2+" dentro de cifrado vignere"
				+ "da como mensaje cifrado "+Descifrado.Vigenere(clave2, mensaje3, alfabeto));
			
				
		//PRUEBAS PARA CIFRADO MONOALFAB�TICO
		String mensaje4 = "carlos";
		String mensajeCifrado = "oltg�v";
		System.out.println("El mensaje "+mensaje4+" cifrado con monoalfabetico da un mensaje cifrado de "+Cifrado.Monoalfabetico(BigInteger.valueOf(2), BigInteger.valueOf(11), mensaje4, alfabeto2));
		System.out.println("El mensaje "+mensajeCifrado+" cifrado con monoalfabetico da un mensaje descifrado de "+Descifrado.Monoalfabetico(BigInteger.valueOf(2), BigInteger.valueOf(11), mensajeCifrado, alfabeto2));
		
		
		System.out.println(Cifrado.Hill("07022008", alfabetoHill, 5, 20, 21, 16));
		System.out.println(Cifrado.Hill("mepillaste", alfabetoHill, 2, 12, 35, 4));
		System.out.println("Pruebas exitosas...");
	}
	*/
	
	/**
	 * Actualiza la lista de usuarios (obtiene los que hay en base de datos).
	 */
	public static void actualizarListaUsuarios() {
		Aplicacion.listaUsuarios = BaseDeDatos.getUsuarios();
	}
	
	/**
	 * Obtiene el alfabeto utilizado en la aplicaci�n.
	 * @return Alfabeto base utilizado.
	 */
	public static String getAlfabeto() {
		return alfabeto;
	}

	public static ArrayList<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

}
