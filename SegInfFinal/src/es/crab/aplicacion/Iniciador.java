package es.crab.aplicacion;

import es.crab.interfaz.PantallaCarga;

/**
 * Clase que inicia la aplicaci�n.
 * @author CRAB
 *
 */
public class Iniciador {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("+--------------------------------------------------------------------------------------------+");
		System.out.println("| DANIEL ALCAL� VALERA - MART�N BAY�N GUTI�RREZ - BEATRIZ CRESPO TORBADO - MARIA RIA�O SALIO |");
		System.out.println("|       Universidad de Le�n - Grado en Ingenier�a Inform�tica - Seguridad Inform�tica        |");
		System.out.println("+--------------------------------------------------------------------------------------------+");
		System.out.println("| Generando n�meros primos hasta 2.000.000 (excluyendo 1 y 2) [Tiempo estimado: 12 segundos] |");
		System.out.println("+--------------------------------------------------------------------------------------------+");
		Aplicacion.actualizarListaUsuarios();
		PantallaCarga inicio = new PantallaCarga();
		
	}

}
